package com.fbMatcher.listeners;

import org.apache.log4j.Logger;
import org.testng.ITestResult;
import org.testng.TestListenerAdapter;

import com.fbMatcher.testBase.InitialiseTestBase;


public class MyTestListener extends TestListenerAdapter{
	
	public static Logger log = Logger.getLogger(MyTestListener.class);
	
	@Override
	public void onTestFailure(ITestResult tresult) {
		
		log.info("Test Run Failed. Cleaning Up.");
		InitialiseTestBase clearUp = new InitialiseTestBase(null);
		clearUp.tearDown();		
	}

}
