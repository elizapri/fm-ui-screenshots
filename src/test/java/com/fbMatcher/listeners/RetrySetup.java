package com.fbMatcher.listeners;

import org.apache.log4j.Logger;
import org.testng.IRetryAnalyzer;
import org.testng.ITestResult;

public class RetrySetup implements IRetryAnalyzer{
	
	//counter for retry
	private int retryCount = 0;
	private static final int maxRetryCount = 1;
	
	public static Logger log = Logger.getLogger(RetrySetup.class);
	
	@Override
	public boolean retry(ITestResult result) {
		if (retryCount < maxRetryCount) {
			retryCount++;
			log.info("Retrying Test Method: "+result.getName()+" for "+retryCount+" times.");
			return true;
		}
		return false;
	}
}
