package com.fbMatcher.listeners;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;

import org.testng.IAnnotationTransformer;
import org.testng.annotations.ITestAnnotation;

public class TransferAnnotation implements IAnnotationTransformer{
	
	@Override
	public void transform(ITestAnnotation annotation, @SuppressWarnings("rawtypes") Class testClass, Constructor testConstructor, Method testMethod) {
			
			annotation.setRetryAnalyzer(RetrySetup.class);
	}
}
