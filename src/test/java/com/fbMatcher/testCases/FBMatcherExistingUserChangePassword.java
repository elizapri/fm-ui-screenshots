package com.fbMatcher.testCases;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.Hashtable;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbMatcher.screens.KickaroundBaseScreen;
import com.fbMatcher.screens.LandingHomeScreen;
import com.fbMatcher.screens.LoginScreen;
import com.fbMatcher.screens.ProfileScreen;
import com.fbMatcher.testBase.InitialiseTestBase;
import com.fbMatcher.utilities.CaptureScreenshot;
import com.fbMatcher.utilities.CommonUtils;

public class FBMatcherExistingUserChangePassword extends InitialiseTestBase {

	public FBMatcherExistingUserChangePassword(String tdSheetName) {
		super("Android_ProfileChangePassword");
	}

	LandingHomeScreen homeScreen;
	LoginScreen loginScreen;
	KickaroundBaseScreen kickaroundBaseScreen;
	ProfileScreen profileScreen;

	@BeforeMethod
	public void startSession() {
		startAppium();
	}

	@Test(dataProvider = "dpTestData")
	public void existingUserChangePassword(Hashtable<String, String> testData) throws IOException {

		String testCaseName = FBMatcherExistingUserChangePassword.class.getSimpleName();
		log.info("Executing Testcase: " + testCaseName + " with device: " + testData.get("Device Name"));
		launchFBMatcher(testData);
		homeScreen = new LandingHomeScreen(driver);
		loginScreen = new LoginScreen(driver);
		kickaroundBaseScreen = new KickaroundBaseScreen(driver);
		profileScreen = new ProfileScreen(driver);
		WebDriverWait wait = new WebDriverWait(driver, 20);
		JavascriptExecutor je = (JavascriptExecutor) driver;

		Date currentTimeStamp = new Date();
		String testCaseFolderPath = System.getProperty("user.dir") + "/Screenshots/" + testCaseName + "_"
				+ CommonUtils.DEVICE_NAME + "_" + currentTimeStamp.toString().replace(":", "_");
		File file = new File(testCaseFolderPath);

		wait.until(ExpectedConditions.elementToBeClickable(homeScreen.headerMain));
		file.mkdir();
		log.info("Created folder");

		CaptureScreenshot.captureFullScreen(file, "FBMatcherLandingScreen");
		je.executeScript("arguments[0].scrollIntoView(true);", homeScreen.buttonOpenMenu);

		// if Mobile view
		if (homeScreen.isOpenMenu()) {
			homeScreen.clickOpenMenu();
		}

		homeScreen.clickLogin();
		je.executeScript("arguments[0].scrollIntoView(true);", loginScreen.titleWelcome);
		CaptureScreenshot.captureFullScreen(file, "LoginInScreen_Before Credentials");
		loginScreen.setEmail(testData.get("Email"));
		loginScreen.setPassword(testData.get("Password"));
		CaptureScreenshot.captureFullScreen(file, "LoginInScreen_After Credentials");
		loginScreen.clickLogin();

		CaptureScreenshot.captureFullScreen(file, "LoginInProcessScreen");
		wait.until(ExpectedConditions.visibilityOf(kickaroundBaseScreen.buttonOpenMenu));
		CaptureScreenshot.captureFullPage(file, "LandingKickaroundScreen");

		// if Mobile view
		if (homeScreen.isOpenMenu()) {
			homeScreen.clickOpenMenu();
		}
		kickaroundBaseScreen.clickMenuUserName();
		kickaroundBaseScreen.clickMenuProfile();
		je.executeScript("arguments[0].scrollIntoView(true);", profileScreen.headerAccountDetails);
		CaptureScreenshot.captureFullPage(file, "ProfileScreen");

		profileScreen.clickChangePassword();
		CaptureScreenshot.captureFullScreen(file, "ChangePasswordScreen");
		profileScreen.clickCancelPassword();
		je.executeScript("arguments[0].scrollIntoView(true);", homeScreen.buttonOpenMenu);

		// if Mobile view
		if (homeScreen.isOpenMenu()) {
			homeScreen.clickOpenMenu();
		}
		profileScreen.clickMenuUserName();
		profileScreen.clickLogout();
		wait.until(ExpectedConditions.visibilityOf(homeScreen.headerMain));
		log.info("Logged Out Successfully");
		CaptureScreenshot.captureFullScreen(file, "LoggedOutScreen");
		log.info("Test case execution completed");
	}

	@AfterMethod
	public void closeSession() {
		tearDown();
	}
}
