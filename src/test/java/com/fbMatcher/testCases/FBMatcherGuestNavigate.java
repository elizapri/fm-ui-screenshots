package com.fbMatcher.testCases;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.Hashtable;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbMatcher.screens.AboutUsScreen;
import com.fbMatcher.screens.BlogsScreen;
import com.fbMatcher.screens.FeaturesScreen;
import com.fbMatcher.screens.HowItWorksScreen;
import com.fbMatcher.screens.LandingHomeScreen;
import com.fbMatcher.screens.LoginScreen;
import com.fbMatcher.testBase.InitialiseTestBase;
import com.fbMatcher.utilities.CaptureScreenshot;
import com.fbMatcher.utilities.CommonUtils;

public class FBMatcherGuestNavigate extends InitialiseTestBase {

	public FBMatcherGuestNavigate(String tdSheetName) {
		super("Android_UserLogin");
	}

	LandingHomeScreen homeScreen;
	FeaturesScreen featuresScreen;
	HowItWorksScreen howitworksScreen;
	BlogsScreen blogsScreen;
	AboutUsScreen aboutusScreen;
	LoginScreen loginScreen;

	@BeforeMethod
	public void startSession() {
		startAppium();
	}

	@Test(dataProvider = "dpTestData")
	public void navigateLandingScreen(Hashtable<String, String> testData) throws IOException {

		String testCaseName = FBMatcherGuestNavigate.class.getSimpleName();

		log.info("Executing Testcase: " + testCaseName + " with device: " + testData.get("Device Name"));

		launchFBMatcher(testData);

		homeScreen = new LandingHomeScreen(driver);
		featuresScreen = new FeaturesScreen(driver);
		howitworksScreen = new HowItWorksScreen(driver);
		blogsScreen = new BlogsScreen(driver);
		aboutusScreen = new AboutUsScreen(driver);
		loginScreen = new LoginScreen(driver);
		WebDriverWait wait = new WebDriverWait(driver, 20);
		JavascriptExecutor je = (JavascriptExecutor) driver;

		Date currentTimeStamp = new Date();
		String testCaseFolderPath = System.getProperty("user.dir") + "/Screenshots/" + testCaseName + "_"
				+ CommonUtils.DEVICE_NAME + "_" + currentTimeStamp.toString().replace(":", "_");
		File file = new File(testCaseFolderPath);

		wait.until(ExpectedConditions.elementToBeClickable(homeScreen.headerMain));
		file.mkdir();
		log.info("Created folder");

		CaptureScreenshot.captureFullScreen(file, "FBMatcherLandingScreen");

		je.executeScript("arguments[0].scrollIntoView(true);", featuresScreen.titleFeatures);
		CaptureScreenshot.captureFullScreen(file, "FeatureScreen");

		je.executeScript("arguments[0].scrollIntoView(true);", howitworksScreen.titleHowItWorks);
		wait.until(ExpectedConditions.visibilityOf(howitworksScreen.titleHowItWorks));
		CaptureScreenshot.captureFullScreen(file, "HowItWorksScreen");

		je.executeScript("arguments[0].scrollIntoView(true);", howitworksScreen.titleSlotOrganiseNow);
		wait.until(ExpectedConditions.visibilityOf(howitworksScreen.titleSlotOrganiseNow));
		CaptureScreenshot.captureFullScreen(file, "SlotsIntoScreen");

		je.executeScript("arguments[0].scrollIntoView(true);", howitworksScreen.titleNeedFB);
		wait.until(ExpectedConditions.visibilityOf(howitworksScreen.titleNeedFB));
		CaptureScreenshot.captureFullScreen(file, "NeedMoreFBScreen");

		je.executeScript("arguments[0].scrollIntoView(true);", howitworksScreen.titleLatestArticles);
		wait.until(ExpectedConditions.visibilityOf(howitworksScreen.titleLatestArticles));
		CaptureScreenshot.captureFullPage(file, "LatestArticlesScreen");
		je.executeScript("arguments[0].scrollIntoView(true);", howitworksScreen.buttonOpenMenu);

		// if Mobile view
		if (homeScreen.isOpenMenu()) {
			homeScreen.clickOpenMenu();
		}

		homeScreen.clickBlogs();
		wait.until(ExpectedConditions.visibilityOf(blogsScreen.titleBlog));
		CaptureScreenshot.captureFullPage(file, "BlogsScreen");
		je.executeScript("arguments[0].scrollIntoView(true);", blogsScreen.buttonOpenMenu);

		// if Mobile view
		if (homeScreen.isOpenMenu()) {
			homeScreen.clickOpenMenu();
		}

		homeScreen.clickAboutUs();
		wait.until(ExpectedConditions.visibilityOf(aboutusScreen.titleAboutUs));
		CaptureScreenshot.captureFullPage(file, "AboutUsScreen");
		je.executeScript("arguments[0].scrollIntoView(true);", aboutusScreen.buttonOpenMenu);

		// if Mobile view
		if (homeScreen.isOpenMenu()) {
			homeScreen.clickOpenMenu();
		}

		homeScreen.clickLogin();
		wait.until(ExpectedConditions.visibilityOf(loginScreen.titleWelcome));
		CaptureScreenshot.captureFullScreen(file, "LoginScreen");
		log.info("Test case execution completed");
	}

	@AfterMethod
	public void closeSession() {
		tearDown();
	}
}
