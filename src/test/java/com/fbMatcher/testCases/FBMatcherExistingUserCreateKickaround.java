package com.fbMatcher.testCases;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.Hashtable;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbMatcher.screens.KickaroundBaseScreen;
import com.fbMatcher.screens.KickaroundGameDetailsScreen;
import com.fbMatcher.screens.KickaroundLocationGroupScreen;
import com.fbMatcher.screens.LandingHomeScreen;
import com.fbMatcher.screens.LoginScreen;
import com.fbMatcher.screens.OrganiseKickaroundCostScreen;
import com.fbMatcher.screens.OrganiseKickaroundWhatScreen;
import com.fbMatcher.screens.OrganiseKickaroundWhenScreen;
import com.fbMatcher.screens.OrganiseKickaroundWhereScreen;
import com.fbMatcher.testBase.InitialiseTestBase;
import com.fbMatcher.utilities.CaptureScreenshot;
import com.fbMatcher.utilities.CommonUtils;

public class FBMatcherExistingUserCreateKickaround extends InitialiseTestBase {

	public FBMatcherExistingUserCreateKickaround(String tdSheetName) {
		super("Android_CreateKickaround");
	}

	LandingHomeScreen homeScreen;
	LoginScreen loginScreen;
	KickaroundBaseScreen kickaroundBaseScreen;
	OrganiseKickaroundWhatScreen kickaroundWhatScreen;
	OrganiseKickaroundWhenScreen kickaroundWhenScreen;
	OrganiseKickaroundWhereScreen kickaroundWhereScreen;
	OrganiseKickaroundCostScreen kickaroundCostScreen;
	KickaroundGameDetailsScreen kickaroundGameScreen;
	KickaroundLocationGroupScreen kickaroundLocationGroupScreen;
	String minPlayerCostValue;
	String maxPlayerCostValue;
	int playerPrice;

	@BeforeMethod
	public void startSession() {
		startAppium();
	}

	@Test(dataProvider = "dpTestData")
	public void createKickaround(Hashtable<String, String> testData) throws IOException {

		String testCaseName = FBMatcherExistingUserCreateKickaround.class.getSimpleName();

		log.info("Executing Testcase: " + testCaseName + " with device: " + testData.get("Device Name"));

		launchFBMatcher(testData);

		homeScreen = new LandingHomeScreen(driver);
		loginScreen = new LoginScreen(driver);
		kickaroundBaseScreen = new KickaroundBaseScreen(driver);
		kickaroundWhatScreen = new OrganiseKickaroundWhatScreen(driver);
		kickaroundWhenScreen = new OrganiseKickaroundWhenScreen(driver);
		kickaroundWhereScreen = new OrganiseKickaroundWhereScreen(driver);
		kickaroundCostScreen = new OrganiseKickaroundCostScreen(driver);
		kickaroundGameScreen = new KickaroundGameDetailsScreen(driver);
		kickaroundLocationGroupScreen = new KickaroundLocationGroupScreen(driver);
		WebDriverWait wait = new WebDriverWait(driver, 20);
		JavascriptExecutor je = (JavascriptExecutor) driver;

		Date currentTimeStamp = new Date();
		String testCaseFolderPath = System.getProperty("user.dir") + "/Screenshots/" + testCaseName + "_"
				+ CommonUtils.DEVICE_NAME + "_" + currentTimeStamp.toString().replace(":", "_");
		File file = new File(testCaseFolderPath);

		wait.until(ExpectedConditions.elementToBeClickable(homeScreen.headerMain));
		file.mkdir();
		log.info("Created folder");

		CaptureScreenshot.captureFullScreen(file, "FBMatcherLandingScreen");
		je.executeScript("arguments[0].scrollIntoView(true);", homeScreen.buttonOpenMenu);

		// if Mobile view
		if (homeScreen.isOpenMenu()) {
			homeScreen.clickOpenMenu();
		}

		homeScreen.clickLogin();
		wait.until(ExpectedConditions.visibilityOf(loginScreen.titleWelcome));
		CaptureScreenshot.captureFullScreen(file, "LoginInScreen_Before Credentials");
		loginScreen.setEmail(testData.get("Email"));
		loginScreen.setPassword(testData.get("Password"));
		CaptureScreenshot.captureFullScreen(file, "LoginInScreen_After Credentials");
		loginScreen.clickLogin();

		CaptureScreenshot.captureFullScreen(file, "LoginInProcessScreen");
		wait.until(ExpectedConditions.visibilityOf(kickaroundBaseScreen.buttonOpenMenu));
		CaptureScreenshot.captureFullPage(file, "LandingKickaroundScreen");

		// if Mobile view
		if (homeScreen.isOpenMenu()) {
			homeScreen.clickOpenMenu();
		}
		kickaroundBaseScreen.clickMenuKickaround();
		kickaroundBaseScreen.createMenuKickaround();

		// Enter Details in Kickaround-What Screen
		log.info("On What screen");
		wait.until(ExpectedConditions.visibilityOf(kickaroundWhatScreen.titleWhat));
		kickaroundWhatScreen.setKickaroundName(testData.get("Name of Kickaround"));
		kickaroundWhatScreen.clickWhatsAppIntegration();
		kickaroundWhatScreen.selectOption(testData.get("WhatsApp Group"));

		if (kickaroundWhatScreen.drpWhatsAppGroup.getText() == "Use Existing Group") {
			kickaroundWhatScreen.setWhatsAppURL(testData.get("WhatsApp Group URL"));
		}
		kickaroundWhatScreen.clickPitchSize();
		kickaroundWhatScreen.selectOption(testData.get("Pitch Size"));
		kickaroundWhatScreen.clickPitchType();
		kickaroundWhatScreen.selectOption(testData.get("Pitch Type"));
		kickaroundWhatScreen.setMinPlayers(testData.get("Minimum Players"));
		kickaroundWhatScreen.setMaxPlayers(testData.get("Maximum Players"));
		kickaroundWhatScreen.clickGender();
		kickaroundWhatScreen.selectOption(testData.get("Gender"));
		kickaroundWhatScreen.clickSkillLevel();
		kickaroundWhatScreen.selectOption(testData.get("Skill Level"));
		CaptureScreenshot.captureFullPage(file, "OrganiseKickaround-WhatScreen");
		kickaroundWhatScreen.clickNext();

		// Enter Details in Kickaround-When Screen
		log.info("On When screen");
		wait.until(ExpectedConditions.visibilityOf(kickaroundWhenScreen.titleWhen));
		kickaroundWhenScreen.clickGameFrequency();
		kickaroundWhenScreen.selectOption(testData.get("Game Frequency"));
		kickaroundWhenScreen.setGameDuration(testData.get("Duration"));

		if ((kickaroundWhenScreen.drpFrequency.getText() != "Adhoc")
				|| (kickaroundWhenScreen.drpFrequency.getText() == "Adhoc"
						&& kickaroundWhenScreen.checkboxCreateGame.isSelected())) {
			kickaroundWhenScreen.clickCalendar();
			kickaroundWhenScreen.selectDate(testData.get("Game Year"), testData.get("Game Month"),
					testData.get("Game Day"));
			kickaroundWhenScreen.setGameHours(testData.get("Game Hour"));
			kickaroundWhenScreen.setGameMinutes(testData.get("Game Mins"));
			kickaroundWhenScreen.clickAMPM(testData.get("AM/PM?"));
		} else {
			kickaroundWhenScreen.uncheckCreateGame();
		}
		CaptureScreenshot.captureFullPage(file, "OrganiseKickaround-WhenScreen");
		kickaroundWhenScreen.clickNext();

		// Enter Details in Kickaround-Where Screen
		log.info("On Where screen");
		CaptureScreenshot.captureFullPage(file, "OrganiseKickaround-FirstWhereScreen");
		wait.until(ExpectedConditions.visibilityOf(kickaroundWhereScreen.titleWhere));
		kickaroundWhereScreen.setLocation(testData.get("Sports Venue"));
		wait.until(ExpectedConditions.visibilityOf(kickaroundWhereScreen.listLocations));
		kickaroundWhereScreen.selectLocation(testData.get("Sports Venue"));
		CaptureScreenshot.captureFullPage(file, "OrganiseKickaround-WhereScreen");
		je.executeScript("arguments[0].scrollIntoView(true);", kickaroundWhereScreen.buttonNext);
		kickaroundWhereScreen.clickNext();

		// Enter Details in Kickaround-Cost Screen
		log.info("On Cost screen");
		wait.until(ExpectedConditions.visibilityOf(kickaroundCostScreen.titleCost));
		kickaroundCostScreen.clickHowToCollect();
		kickaroundWhenScreen.selectOption(testData.get("How to Collect"));

		if (!(kickaroundCostScreen.drpHowCollect.getText().equals("No charge, its free"))) {
			kickaroundCostScreen.clickPriceType();
			kickaroundWhenScreen.selectOption(testData.get("Price Type"));
			log.info(kickaroundCostScreen.drpHowPriceType.getText());

			if (kickaroundCostScreen.drpHowPriceType.getText().equals("Fixed player price")) {
				log.info("Got into Fixed");
				kickaroundCostScreen.setFixedCost(testData.get("Fixed Player Price"));
				kickaroundCostScreen.setDiscountCode(testData.get("Coupon code"));

				if (!(kickaroundCostScreen.textDiscountCode.getText().equals("DEMO"))) {
					kickaroundCostScreen.setDiscountCode(testData.get("Coupon code"));
				}
				wait.until(ExpectedConditions.visibilityOf(kickaroundCostScreen.titlePriceBreakdown));
			} else {
				log.info("Got into Variable");
				kickaroundCostScreen.setVariableCost(testData.get("Variable Player Price"));
				kickaroundCostScreen.setDiscountCode(testData.get("Coupon code"));

				if (!(kickaroundCostScreen.textDiscountCode.getText().equals("DEMO"))) {
					kickaroundCostScreen.setDiscountCode(testData.get("Coupon code"));
				}
				wait.until(ExpectedConditions.visibilityOf(kickaroundCostScreen.titlePriceBreakdown));
			}
		}
		CaptureScreenshot.captureFullPage(file, "OrganiseKickaround-CostScreen");
		kickaroundCostScreen.clickCreate();
		wait.until(ExpectedConditions.visibilityOf(kickaroundGameScreen.titleKickaroundName));
		log.info("On Game Details Screen");
		CaptureScreenshot.captureFullPage(file, "OrganiseKickaround-GameDetailsScreen");
		kickaroundGameScreen.clickLocationGroup();
		wait.until(ExpectedConditions.visibilityOf(kickaroundLocationGroupScreen.titleGame));
		log.info("On Game Location and Group Screen");
		CaptureScreenshot.captureFullPage(file, "OrganiseKickaround-GameLocationGroupScreen");

		je.executeScript("arguments[0].scrollIntoView(true);", kickaroundLocationGroupScreen.buttonOpenMenu);

		// if Mobile view
		if (homeScreen.isOpenMenu()) {
			homeScreen.clickOpenMenu();
		}

		kickaroundLocationGroupScreen.clickMenuUserName();
		kickaroundLocationGroupScreen.clickLogout();
		wait.until(ExpectedConditions.visibilityOf(homeScreen.headerMain));
		log.info("Logged Out Successfully");
		CaptureScreenshot.captureFullScreen(file, "LoggedOutScreen");
		log.info("Test case execution completed");
	}

	@AfterMethod
	public void closeSession() {
		tearDown();
	}
}
