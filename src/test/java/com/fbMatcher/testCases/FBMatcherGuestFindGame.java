package com.fbMatcher.testCases;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.Hashtable;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbMatcher.screens.FindLocalFootBallAvailabilityConfirmationScreen;
import com.fbMatcher.screens.FindLocalFootBallGetNotifiedScreen;
import com.fbMatcher.screens.FindLocalFootBallWhenScreen;
import com.fbMatcher.screens.FindLocalFootBallWhereScreen;
import com.fbMatcher.screens.LandingHomeScreen;
import com.fbMatcher.testBase.InitialiseTestBase;
import com.fbMatcher.utilities.CaptureScreenshot;
import com.fbMatcher.utilities.CommonUtils;

public class FBMatcherGuestFindGame extends InitialiseTestBase {

	public FBMatcherGuestFindGame(String tdSheetName) {
		super("Android_FindGame");
	}

	LandingHomeScreen homeScreen;
	FindLocalFootBallWhereScreen findGameWhereScreen;
	FindLocalFootBallWhenScreen findGameWhenScreen;
	FindLocalFootBallGetNotifiedScreen findGameNotifyScreen;
	FindLocalFootBallAvailabilityConfirmationScreen findGameConfirmScreen;
	
	@BeforeMethod
	public void startSession() {
		startAppium();
	}

	@Test(dataProvider = "dpTestData")
	public void findGame(Hashtable<String, String> testData) throws IOException {
		
		String testCaseName = FBMatcherGuestFindGame.class.getSimpleName();

		log.info("Executing Testcase: " + testCaseName + " with device: " + testData.get("Device Name"));

		launchFBMatcher(testData);

		homeScreen = new LandingHomeScreen(driver);
		findGameWhereScreen = new FindLocalFootBallWhereScreen(driver);
		findGameWhenScreen = new FindLocalFootBallWhenScreen(driver);
		findGameNotifyScreen = new FindLocalFootBallGetNotifiedScreen(driver);
		findGameConfirmScreen = new FindLocalFootBallAvailabilityConfirmationScreen(driver);
		WebDriverWait wait = new WebDriverWait(driver, 20);
		JavascriptExecutor je = (JavascriptExecutor) driver;

		Date currentTimeStamp = new Date();
		String testCaseFolderPath = System.getProperty("user.dir") + "/Screenshots/" + testCaseName + "_"
				+ CommonUtils.DEVICE_NAME + "_" + currentTimeStamp.toString().replace(":", "_");
		File file = new File(testCaseFolderPath);

		wait.until(ExpectedConditions.elementToBeClickable(homeScreen.headerMain));
		file.mkdir();
		log.info("Created folder");

		CaptureScreenshot.captureFullScreen(file, "FBMatcherLandingScreen");
		je.executeScript("arguments[0].scrollIntoView(true);", homeScreen.buttonOpenMenu);

		homeScreen.clickFindGame();

		// Enter Details in Find Game-Where Screen
		log.info("On Where screen");
		wait.until(ExpectedConditions.visibilityOf(findGameWhereScreen.titleWhere));
		findGameWhereScreen.setLocation(testData.get("Location"));
		findGameWhereScreen.enter();
		findGameWhereScreen.slideMiles(testData.get("How Far Travel"));
		wait.until(ExpectedConditions.visibilityOf(findGameWhereScreen.tableLocations));
		CaptureScreenshot.captureFullPage(file, "FindGame-WhereScreen");
		findGameWhenScreen.clickTitleWhen();

		// Enter Details in Find Game-When Screen
		log.info("On When screen");
		wait.until(ExpectedConditions.visibilityOf(findGameWhenScreen.titleWhen));
		findGameWhenScreen.clickLocations();
		findGameWhenScreen.selectLocation(testData.get("Locations"));
		findGameWhenScreen.tab();
		findGameWhenScreen.clickDaysOfWeek();
		findGameWhenScreen.selectDaysOfWeek(testData.get("Days of Week"));
		findGameWhenScreen.tab();
		// leaving slider values as it is for now
		findGameWhenScreen.clickAddAvailability();
		wait.until(ExpectedConditions.visibilityOf(findGameWhenScreen.tableAvailability));
		CaptureScreenshot.captureFullPage(file, "FindGame-WhenScreen");
		findGameNotifyScreen.clickTitleNotify();

		// Enter Details in Find Game-Notify Screen
		log.info("On Notify screen");
		wait.until(ExpectedConditions.visibilityOf(findGameNotifyScreen.titleNotify));
		findGameNotifyScreen.setFirstName(testData.get("First Name"));
		findGameNotifyScreen.setLastName(testData.get("Last Name"));
		findGameNotifyScreen.setEmail(testData.get("Email"));
		CaptureScreenshot.captureFullPage(file, "FindGame-NotifyScreen");
		findGameNotifyScreen.clickSignUp();

		// Verify confirmation screen is displayed
		log.info("On Confirmation screen");
		findGameConfirmScreen.isTitleConfirmation();
		CaptureScreenshot.captureFullPage(file, "FindGame-ConfirmScreen");
		findGameConfirmScreen.clickClose();

		wait.until(ExpectedConditions.visibilityOf(homeScreen.headerMain));
		CaptureScreenshot.captureFullScreen(file, "FBMatcherLandingScreen_After");
		log.info("Test case execution completed");
	}

	@AfterMethod
	public void closeSession() {
		tearDown();
	}

}
