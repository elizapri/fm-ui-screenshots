package com.fbMatcher.utilities;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Hashtable;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.remote.MobileCapabilityType;

public class CommonUtils {

	private static AppiumDriver<MobileElement> driver;
	private static URL serverURL;
	public static String APPIUM_PORT;
	private static String AUTOMATION_NAME;
	private static String BROWSER_NAME;
	private static String PLATFORM_NAME;
	private static String PLATFORM_VERSION;
	public static String DEVICE_NAME;
	private static String UDID;
	public static String APPLICATION_URL;
	private static int NEW_COMMAND_TIMEOUT;
	private static String AVD;
	private static int AVD_LAUNCH_TIMEOUT;
	private static int AVD_READY_TIMEOUT;
	private static int UI_AUTOMATOR2SERVER_LAUNCH_TIMEOUT;
	private static int UI_AUTOMATOR2SERVER_INSTALL_TIMEOUT;
	private static int ADB_EXEC_TIMEOUT;	
	public static String DPR;
	public static boolean HEADLESS;

	private static DesiredCapabilities capabilities = new DesiredCapabilities();
	private static Properties prop = new Properties();
	public static Logger log = Logger.getLogger(CommonUtils.class);	
		
	public static void loadAndroidConfigData(Hashtable<String,String> configData) {	
		
		APPIUM_PORT = configData.get("Appium Server Port");
		APPLICATION_URL =  configData.get("Application URL");
				
		AUTOMATION_NAME = configData.get("Automation Name");
		BROWSER_NAME = configData.get("Browser");
		PLATFORM_NAME = configData.get("Platform Name");
		PLATFORM_VERSION = configData.get("Platform Version");
		DEVICE_NAME = configData.get("Device Name");		
		NEW_COMMAND_TIMEOUT = Integer.parseInt(configData.get("New Command Timeout"));
		AVD = configData.get("AVD Name");
		AVD_LAUNCH_TIMEOUT = Integer.parseInt(configData.get("AVD Launch Timeout"));
		AVD_READY_TIMEOUT = Integer.parseInt(configData.get("AVD Ready Timeout"));
		UI_AUTOMATOR2SERVER_LAUNCH_TIMEOUT = Integer.parseInt(configData.get("UIAutomator2Server Launch Timeout"));
		UI_AUTOMATOR2SERVER_INSTALL_TIMEOUT = Integer.parseInt(configData.get("UIAutomator2Server Install Timeout"));
		ADB_EXEC_TIMEOUT = Integer.parseInt(configData.get("ADB Execution Timeout"));
		DPR = configData.get("DPR");
		HEADLESS = Boolean.parseBoolean(configData.get("Headless"));
	}

	// Setting Android Capabilities
	public static void setAndroidCapabilities() {
		capabilities.setCapability(MobileCapabilityType.AUTOMATION_NAME, AUTOMATION_NAME);
		capabilities.setCapability(MobileCapabilityType.PLATFORM_NAME, PLATFORM_NAME);
		capabilities.setCapability(MobileCapabilityType.PLATFORM_VERSION, PLATFORM_VERSION);
		capabilities.setCapability(MobileCapabilityType.BROWSER_NAME, BROWSER_NAME);
		capabilities.setCapability(MobileCapabilityType.NO_RESET, true);
		capabilities.setCapability("avd", AVD);
		capabilities.setCapability("avdLaunchTimeout", AVD_LAUNCH_TIMEOUT);
		capabilities.setCapability("avdReadyTimeout", AVD_READY_TIMEOUT);
		capabilities.setCapability(MobileCapabilityType.NEW_COMMAND_TIMEOUT, NEW_COMMAND_TIMEOUT);
		capabilities.setCapability("adbExecTimeout", ADB_EXEC_TIMEOUT);
		capabilities.setCapability("appium:uiautomator2ServerLaunchTimeout", UI_AUTOMATOR2SERVER_LAUNCH_TIMEOUT);
		capabilities.setCapability("appium:uiautomator2ServerInstallTimeout", UI_AUTOMATOR2SERVER_INSTALL_TIMEOUT);
		capabilities.setCapability("autoGrantPermissions", true);
		capabilities.setCapability("noSign", true);
		capabilities.setCapability("appium:showChromedriverLog", true);
		capabilities.setCapability("isHeadless", HEADLESS);
		capabilities.setCapability("appium:ignoreHiddenApiPolicyError", true);
		capabilities.setCapability("unicodeKeyboard", true);
		capabilities.setCapability("resetKeyboard", true);
		capabilities.setCapability("chromedriverExecutableDir",System.getProperty("user.dir") + "/src/test/resources/chromedriver/");
		
		ChromeOptions chromeOptions = new ChromeOptions();
		chromeOptions.setExperimentalOption("w3c", false);	
		chromeOptions.addArguments("no-sandbox");
		chromeOptions.addArguments("--no-first-run");
		capabilities.merge(chromeOptions);
	}

	public static AppiumDriver<MobileElement> getAndroidDriver() {
		try {
			serverURL = new URL("http://localhost:"+APPIUM_PORT+"/wd/hub/");
			driver = new AndroidDriver<MobileElement>(serverURL, capabilities);
			log.info("Driver started");
			
		} catch (MalformedURLException e) {
			log.info("Arguments to URL is incorrect or null OR Unknown protocol");
			e.printStackTrace();
		}	catch (Exception e ) {
			log.info("Exception in Driver instantiation");
			e.printStackTrace();
		}	catch (Throwable e)	{
			log.info("Chrome not reachable");
			e.printStackTrace();;
		}
		return driver;
	}

	public static void loadiOSConfigData(Hashtable<String,String> testData) {
		
		APPIUM_PORT = prop.getProperty("appium.server.port");
		AUTOMATION_NAME = prop.getProperty("automation.name");
		BROWSER_NAME = prop.getProperty("browser.name");
		PLATFORM_NAME = prop.getProperty("platform.name");
		PLATFORM_VERSION = prop.getProperty("platform.version");
		DEVICE_NAME = prop.getProperty("device.name");
		UDID = prop.getProperty("udid");
		APPLICATION_URL = prop.getProperty("application.url");
		NEW_COMMAND_TIMEOUT = Integer.parseInt(prop.getProperty("newcommand.timeout"));
		DPR = prop.getProperty("dpr");
	}

	// Setting iOS Capabilities
	public static void setiOSCapabilities() {
		capabilities.setCapability(MobileCapabilityType.AUTOMATION_NAME, AUTOMATION_NAME);
		capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, DEVICE_NAME);
		capabilities.setCapability(MobileCapabilityType.UDID, UDID);
		capabilities.setCapability(MobileCapabilityType.PLATFORM_NAME, PLATFORM_NAME);
		capabilities.setCapability(MobileCapabilityType.PLATFORM_VERSION, PLATFORM_VERSION);
		capabilities.setCapability(MobileCapabilityType.BROWSER_NAME, BROWSER_NAME);
		capabilities.setCapability(MobileCapabilityType.NO_RESET, true);
		capabilities.setCapability("autoGrantPermissions", true);
		capabilities.setCapability(MobileCapabilityType.NEW_COMMAND_TIMEOUT, NEW_COMMAND_TIMEOUT);
	}

	public static AppiumDriver<MobileElement> getiOSDriver() {
		try {
			serverURL = new URL("http://localhost:" +APPIUM_PORT+ "/wd/hub/");			
		} catch (MalformedURLException e) {
			log.info("Arguments to URL is incorrect or null OR Unknown protocol");
			e.printStackTrace();
		}
		driver = new IOSDriver<MobileElement>(serverURL, capabilities);
		return driver;
	}

}
