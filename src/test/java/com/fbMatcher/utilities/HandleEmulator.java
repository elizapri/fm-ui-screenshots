package com.fbMatcher.utilities;

import java.io.IOException;

import org.apache.log4j.Logger;

public class HandleEmulator {

	public static Logger log = Logger.getLogger(HandleEmulator.class);

	public static boolean killEmulator() {

		try {
			ProcessBuilder processBuilder = new ProcessBuilder();
			processBuilder.command("cmd", "/c", "adb", "-e", "emu", "kill");
			Process processEmulator = processBuilder.start();

			if (processEmulator.waitFor() == 0) {
				return true;
			}

		} catch (InterruptedException e) {
			log.info("Device/Emulator process could not be killed");
			e.printStackTrace();
		} catch (IOException e) {
			log.info("Device/Emulator process could not be killed");
			e.printStackTrace();
		}
		return false;
	}	
}
