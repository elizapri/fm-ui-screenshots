package com.fbMatcher.utilities;

import java.io.File;
import java.io.IOException;
import java.util.Date;

import javax.imageio.ImageIO;

import com.fbMatcher.base.ScreenBase;

import ru.yandex.qatools.ashot.AShot;
import ru.yandex.qatools.ashot.Screenshot;
import ru.yandex.qatools.ashot.shooting.ShootingStrategies;

public class CaptureScreenshot {
	
	private static float dpr = Float.parseFloat(CommonUtils.DPR);
	
	public static void captureFullScreen(File file , String screenName) throws IOException {
		Screenshot screenshot = new AShot().shootingStrategy(ShootingStrategies.scaling(dpr)).takeScreenshot(ScreenBase.driver);
		Date currentTimeStamp = new Date();		
		ImageIO.write(screenshot.getImage(), "PNG", new File(file+"/"+screenName+"_"+currentTimeStamp.toString().replace(":", "_")+".png"));
	}
	
	public static void captureFullPage(File file , String screenName) throws IOException {
		Screenshot screenshot = new AShot().shootingStrategy(ShootingStrategies.viewportRetina(1000, 0, 0, dpr)).takeScreenshot(ScreenBase.driver);
		Date currentTimeStamp = new Date();		
		ImageIO.write(screenshot.getImage(), "PNG", new File(file+"/"+screenName+"_"+currentTimeStamp.toString().replace(":", "_")+".png"));
	}
}