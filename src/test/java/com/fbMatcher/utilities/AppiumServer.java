package com.fbMatcher.utilities;

import java.io.File;
import java.net.URL;
import java.time.Duration;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;

import io.appium.java_client.service.local.AppiumDriverLocalService;
import io.appium.java_client.service.local.AppiumServiceBuilder;
import io.appium.java_client.service.local.flags.GeneralServerFlag;
import org.openqa.selenium.net.UrlChecker;


public class AppiumServer {
	
	public static Logger log = Logger.getLogger(AppiumServer.class);
	
	public static AppiumDriverLocalService service;
	
		public static void start(){
		
			// Start the Appium server code
		 	service = AppiumDriverLocalService.buildService(
					new AppiumServiceBuilder().usingDriverExecutable(new File("C:\\Program Files\\nodejs\\node.exe"))
					.withAppiumJS(new File("C:\\Users\\admin\\AppData\\Roaming\\npm\\node_modules\\appium\\build\\lib\\main.js"))
					.withArgument(GeneralServerFlag.LOCAL_TIMEZONE));
					service.start();							
		}
		
		public static void stop(){
			service.stop();		
		}
		
		
		//to check if Appium server port is in use
		private static final String SERVER_URL = String.format("http://127.0.0.1:%d/wd/hub", CommonUtils.APPIUM_PORT);

	    public boolean waitUntilAppiumIsRunning(long timeout) throws Exception {
	        final URL status = new URL(SERVER_URL + "/sessions");
	        try {
	            new UrlChecker().waitUntilAvailable(timeout, TimeUnit.MILLISECONDS, status);
	            return true;
	        } catch (UrlChecker.TimeoutException e) {
	            return false;
	        }
	    }
		
}
		
		//Other way to start Appium	to auto download chrome driver
	
	/*public static void start() {
		CommandLine command = new CommandLine("C:\\Program Files\\nodejs\\node.exe");
		command.addArgument("C:\\Users\\admin\\AppData\\Roaming\\npm\\node_modules\\appium\\build\\lib\\main.js");
		command.addArgument("--address");
		command.addArgument("127.0.0.1");
		command.addArgument("--port");
		command.addArgument("4723");
		command.addArgument("--allow-insecure=chromedriver_autodownload");
		
				
		DefaultExecuteResultHandler handler = new DefaultExecuteResultHandler();
		DefaultExecutor executor = new DefaultExecutor();
		executor.setExitValue(1);		
		try {
			executor.execute(command, handler);
		} catch (ExecuteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
	}	
	
	public static void stop() {
		Runtime runtime = Runtime.getRuntime();
		try {
			runtime.exec("taskkill /F /IM node.exe");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}*/


