package com.fbMatcher.utilities;

import java.io.IOException;

import org.apache.log4j.Logger;

public class ADBServer {
	
	public static Logger log = Logger.getLogger(ADBServer.class);

	public static boolean stopADB() {

		try {
			ProcessBuilder processBuilder = new ProcessBuilder();
			processBuilder.command("cmd", "/c", "adb", "kill-server");
			Process processADB = processBuilder.start();

			if (processADB.waitFor() == 0) {
				return true;
			}
		} catch (IOException e) {
			log.info("ADB server could not be stopped");
			e.printStackTrace();
		} catch (InterruptedException e) {
			log.info("ADB server could not be stopped");
			e.printStackTrace();
		}

		return false;
	}

	public static boolean restartADB() {
		try {
			ProcessBuilder processBuilder = new ProcessBuilder();
			processBuilder.command("cmd", "/c", "adb", "start-server");
			Process processADB = processBuilder.start();

			if (processADB.waitFor() == 0) {
				return true;
			}

		} catch (IOException e) {
			log.info("ADB server could not be started");
			e.printStackTrace();
		} catch (InterruptedException e) {
			log.info("ADB server could not be started");
			e.printStackTrace();
		}

		return false;
	}

}
