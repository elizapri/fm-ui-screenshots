package com.fbMatcher.testBase;

import java.util.Hashtable;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.testng.annotations.DataProvider;

import com.fbMatcher.utilities.ADBServer;
import com.fbMatcher.utilities.AppiumServer;
import com.fbMatcher.utilities.CommonUtils;
import com.fbMatcher.utilities.ExcelReader;
import com.fbMatcher.utilities.HandleEmulator;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;

public class InitialiseTestBase {

	public AppiumDriver<MobileElement> driver;
	public static ExcelReader excel = null;
	public int excelRowCount;
	public int excelRowNum;
	public int excelColCount;
	public int excelColNum;
	public String sheetName;
	public String deviceStatus;
	public String filePath = System.getProperty("user.dir")
			+ "\\src\\test\\resources\\TestData\\TestExecution_FBMatcher_Testdata.xlsx";
	public static Logger log = Logger.getLogger(InitialiseTestBase.class);

	public InitialiseTestBase(String tdSheetName) {
		sheetName = tdSheetName;
	}

	public void startAppium() {
		PropertyConfigurator
				.configure(System.getProperty("user.dir") + "\\src\\test\\resources\\Properties\\log4j.properties");
		AppiumServer.start();
		log.info("Appium Server started");
	}

	public void stopAppium() {
		AppiumServer.stop();
	}

	public void launchFBMatcher(Hashtable<String, String> configData) {
		log.info("Entered Driver creation method");

		if (sheetName.startsWith("Android")) {
			CommonUtils.loadAndroidConfigData(configData);
			CommonUtils.setAndroidCapabilities();
			driver = CommonUtils.getAndroidDriver();

		} else if (sheetName.startsWith("iOS")) {

			// CommonUtils.loadiOSConfigData();
			// CommonUtils.setiOSCapabilities();
			// driver = CommonUtils.getiOSDriver();
		}

		try {
			driver.get(CommonUtils.APPLICATION_URL);
			log.info("Launched Browser");
		} catch (Exception e) {
			log.info("Exception in website launch due to null driver");
			e.printStackTrace();
			tearDown();
		}
	}

	public void tearDown() {
		driver.quit();
		log.info("quit Driver");
		if (HandleEmulator.killEmulator()) {
			log.info("Device/Emulator killed");
			if (ADBServer.stopADB()) {
				log.info("ADB server stopped");
				stopAppium();
				log.info("Appium Server stopped");
			}
		}
	}

	@DataProvider(name = "dpTestData")
	public Object[][] getTestData() {
		log.info("SheetName is " + sheetName);
		Object[][] testData = getExcelData(filePath, sheetName);
		return testData;
	}

	public Object[][] getExcelData(String path, String sheetName) {
		if (excel == null) {
			excel = new ExcelReader(path, sheetName);
		}
		excelRowCount = excel.getRowCount(sheetName);
		log.info("Total Row Count " + excelRowCount);
		excelColCount = excel.getColumnCount(sheetName);
		log.info("Toltal Column Count " + excelColCount);

		Object[][] excelData = new Object[excelRowCount - 1][1];
		Hashtable<String, String> excelDataTable = null;

		for (excelRowNum = 2; excelRowNum <= excelRowCount; excelRowNum++) {
			log.info(excelRowNum);
			log.info("Inside for loop for test data retrieval");
			excelDataTable = new Hashtable<String, String>();
			for (excelColNum = 0; excelColNum < excelColCount; excelColNum++) {
				excelDataTable.put(excel.getCellData(sheetName, excelColNum, 1),
						excel.getCellData(sheetName, excelColNum, excelRowNum));
				excelData[excelRowNum - 2][0] = excelDataTable;
			}
		}
		return excelData;
	}
}
