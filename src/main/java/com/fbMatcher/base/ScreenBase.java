package com.fbMatcher.base;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.ui.WebDriverWait;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.HasOnScreenKeyboard;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.nativekey.AndroidKey;
import io.appium.java_client.android.nativekey.KeyEvent;

public class ScreenBase {
	
	public static AppiumDriver<MobileElement> driver;
	public WebDriverWait wait;
	
	public ScreenBase(AppiumDriver<MobileElement> driver) {		
		ScreenBase.driver = driver;		
	}
	
	public boolean isKeyboardOpen() {		
		return ((HasOnScreenKeyboard) driver).isKeyboardShown();		
	}
	
	public void hideKeyboard() {
		driver.hideKeyboard();
	}
	
	public void enter() {
		((AndroidDriver<MobileElement>) driver).pressKey(new KeyEvent(AndroidKey.ENTER)); //whenever we need to press Enter
	}
	
	public void tab() {
		((AndroidDriver<MobileElement>) driver).pressKey(new KeyEvent(AndroidKey.TAB)); //whenever we need to press Tab key
	}
	
	//FB Matcher header available for all screens
	@FindBy(xpath="//img[@class='navbar-brand top-menu-logo']")
	public WebElement ImgTopMenuLogo;
	
	public boolean isTopMenuLogo() {
		return ImgTopMenuLogo.isDisplayed();
	}
	
	public void clickTopMenuLogo() {
		ImgTopMenuLogo.click();
	}
	
	//Toggle Menu button is available for all screens
	@FindBy(xpath="//button[@class='navbar-toggler']") //need to move to common testcase
	public WebElement buttonOpenMenu;
	
	public void clickOpenMenu() {
		buttonOpenMenu.click();
	}
	
	public boolean isOpenMenu() {
		return buttonOpenMenu.isDisplayed();
	}
	
	//Chat Option available for all screens
	@FindBys
	({
		@FindBy(xpath="/html/body/div/div[1]/span[2]/div/button"),
		@FindBy(tagName="button")
	})
	public WebElement buttonChat;
		
	public void clickChat() {
		buttonChat.click();
	}	
}

