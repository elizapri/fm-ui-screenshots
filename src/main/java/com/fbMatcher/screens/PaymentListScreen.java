package com.fbMatcher.screens;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class PaymentListScreen extends KickaroundBaseScreen {

	// constructor from superclass
	public PaymentListScreen(AppiumDriver<MobileElement> driver) {
		super(driver);
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	}
	
	//locating the web elements on Payments List Screen
	@FindBy(xpath = "//fm-payments-detail/mat-card/mat-tab-group/mat-tab-header/div[2]/div/div/div/div")
	public WebElement headerPayments;
	
	// defining functionalities of the chosen Web elements
	public boolean isHeaderPayments() {
		return headerPayments.isDisplayed();
	}
	

}
