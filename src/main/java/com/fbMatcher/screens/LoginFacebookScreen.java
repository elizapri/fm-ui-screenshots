package com.fbMatcher.screens;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.fbMatcher.base.ScreenBase;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class LoginFacebookScreen extends ScreenBase {

	// constructor from superclass
	public LoginFacebookScreen(AppiumDriver<MobileElement> driver) {
		super(driver);
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	}

	// locating the web elements on Facebook Login Screen
	@FindBy(xpath = "//div[text()='Log in to Facebook']")
	public WebElement titleFacebook;

	@FindBy(xpath = "//input[@type='text'][@name='email']")
	public WebElement textEmail;

	@FindBy(xpath = "//input[@type='password']")
	public WebElement textPassword;

	@FindBy(xpath = "//button[@type='submit'][text()='Log In']")
	public WebElement buttonLogin;

	@FindBy(xpath = "//a/span[text()='Not now']")
	public WebElement linkNotNow;

	// defining functionalities of the chosen Web elements
	public boolean isTitleFacebook() {
		return titleFacebook.isDisplayed();
	}

	public void setEmail(String email) {
		textEmail.sendKeys(email);
	}

	public void setPassword(String password) {
		textPassword.sendKeys(password);
	}

	public void clickFaceBookLogin() {
		buttonLogin.click();
	}

	public void clickNotNow() {
		linkNotNow.click();
	}

}
