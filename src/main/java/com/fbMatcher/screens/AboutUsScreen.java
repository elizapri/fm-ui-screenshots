package com.fbMatcher.screens;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.fbMatcher.base.ScreenBase;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;


public class AboutUsScreen extends ScreenBase{
	
	//constructor from superclass
	public AboutUsScreen(AppiumDriver<MobileElement> driver) {
		super(driver);
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	}
	
	//locating the web elements on AboutUs Screen
	@FindBy(xpath="//div[@class='blog-title']")
	public WebElement titleAboutUs;
	
	@FindBy(xpath="//span[@class='ng-star-inserted'][1]/a")
	public WebElement breadcrumbHome;
	
	@FindBy(xpath="//span[@class='ng-star-inserted'][2]/a")
	public WebElement breadcrumbAbout;
	
	@FindBy(xpath="//fm-blog-section[2]/div/div")
	public WebElement sectionImages;
	
	//defining functionalities of the chosen Web elements	
	public boolean isTitleAboutUs() {
		return titleAboutUs.isDisplayed();
	}
	
	public void clickBreadcrumbHome() {
		breadcrumbHome.click();
	}
	
	public void clickBreadcrumbAbout() {
		breadcrumbAbout.click();
	}
}