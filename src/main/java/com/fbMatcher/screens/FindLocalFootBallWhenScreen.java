package com.fbMatcher.screens;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.fbMatcher.base.ScreenBase;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class FindLocalFootBallWhenScreen extends ScreenBase {

	// constructor from superclass
	public FindLocalFootBallWhenScreen(AppiumDriver<MobileElement> driver) {
		super(driver);
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	}

	// locating the web elements on Find Game When Screen
	@FindBy(xpath = "//div[3]/div[contains(text(),'When')]")
	public WebElement titleWhen;

	@FindBy(xpath = "//fm-input-availability/div[1]/div[1]/div/mat-form-field/div/div[1]/div/mat-select")
	public WebElement drpLocations;

	@FindBy(xpath = "//fm-input-availability/div[1]/div[2]/div/mat-form-field/div/div[1]/div/mat-select")
	public WebElement drpDaysOfWeek;

	@FindBy(xpath = "//div[@role='listbox']")
	public WebElement listLocations;

	@FindBy(xpath = "//div[@role='listbox']")
	public WebElement listDaysOfWeek;

	@FindBy(xpath = "//fm-input-availability/div[2]/div/nouislider/div/div[1]/div[2]/div")
	public WebElement sliderFrom;

	@FindBy(xpath = "//fm-input-availability/div[2]/div/nouislider/div/div[1]/div[3]/div")
	public WebElement sliderTo;

	@FindBy(xpath = "//fm-input-availability/div[3]/div/button")
	public WebElement buttonAddAvailability;

	@FindBy(xpath = "//form/div/div[2]/h3[contains(text(),'Your Availability')]")
	public WebElement titleAvailability;

	@FindBy(xpath = "//form/div/div[2]/fm-list-availability")
	public WebElement tableAvailability;

	@FindBy(xpath = "(//button[@type='button'][contains(text(),'Back')])[1]")
	public WebElement buttonBack;

	@FindBy(xpath = "(//button[contains(text(), 'Next')])[2]")
	public WebElement buttonNext;

	// defining functionalities of the chosen Web elements
	public boolean isTitleWhen() {
		return titleWhen.isDisplayed();
	}

	public void clickTitleWhen() {
		titleWhen.click();
	}

	public void clickLocations() {
		drpLocations.click();
	}

	public void clickDaysOfWeek() {
		drpDaysOfWeek.click();
	}

	public void selectLocation(String gameLocation) {
		List<WebElement> optionsToSelect = listLocations.findElements(By.tagName("span"));
		for (WebElement option : optionsToSelect) {
			if (option.getText().equals(gameLocation)) {
				option.click();
				break;
			}
		}
	}
	
	public void selectDaysOfWeek(String daysOfWeek) {
		String[] dayOfWeek = daysOfWeek.split(",");
		List<WebElement> optionsToSelect = listDaysOfWeek.findElements(By.tagName("span"));
		System.out.println("Array size: "+dayOfWeek.length);
		System.out.println("Array size from site: "+optionsToSelect.size());
		for (WebElement option : optionsToSelect) {
			for (int i = 0; i < dayOfWeek.length; i++) {
				if (option.getText().equals(dayOfWeek[i])) {
					System.out.println("option from site: "+option.getText()+"User data: "+dayOfWeek[i]);
					option.click();
				}
			}
		}
	}

	//slider login needs re-verification - the value passed needs to change
	public void slideFromTime(String fromTime) {

		String defaultStartTime = sliderFrom.getAttribute("aria-valuenow");
		if (Integer.parseInt(fromTime) > Integer.parseInt(defaultStartTime)) {
			int count = (int) Math.round((Integer.parseInt(fromTime) - Integer.parseInt(defaultStartTime)) / 2.8);
			Actions action = new Actions(driver);
			action.click(sliderFrom).build().perform();
			for (int i = 0; i < count; i++) {
				action.sendKeys(Keys.ARROW_RIGHT).build().perform();
			}
		} else if (Integer.parseInt(fromTime) < Integer.parseInt(defaultStartTime)) {
			int count = (int) Math.round((Integer.parseInt(defaultStartTime) - Integer.parseInt(fromTime)) / 2.8);
			Actions action = new Actions(driver);
			action.click(sliderFrom).build().perform();
			for (int i = 0; i < count; i++) {
				action.sendKeys(Keys.ARROW_LEFT).build().perform();
			}
		}
	}

	public void slideToTime(String toTime) {

		String defaultStartTime = sliderTo.getAttribute("aria-valuenow");
		if (Integer.parseInt(toTime) > Integer.parseInt(defaultStartTime)) {
			int count = (int) Math.round((Integer.parseInt(toTime) - Integer.parseInt(defaultStartTime)) / 2.8);
			Actions action = new Actions(driver);
			action.click(sliderTo).build().perform();
			for (int i = 0; i < count; i++) {
				action.sendKeys(Keys.ARROW_RIGHT).build().perform();
			}
		} else if (Integer.parseInt(toTime) < Integer.parseInt(defaultStartTime)) {
			int count = (int) Math.round((Integer.parseInt(defaultStartTime) - Integer.parseInt(toTime)) / 2.8);
			Actions action = new Actions(driver);
			action.click(sliderTo).build().perform();
			for (int i = 0; i < count; i++) {
				action.sendKeys(Keys.ARROW_LEFT).build().perform();
			}
		}
	}

	public void clickAddAvailability() {
		buttonAddAvailability.click();
	}

	public boolean isTitleAvailability() {
		return titleAvailability.isDisplayed();
	}

	public void clickBack() {
		buttonBack.click();
	}

	public void clickNext() {
		buttonNext.click();
	}
}
