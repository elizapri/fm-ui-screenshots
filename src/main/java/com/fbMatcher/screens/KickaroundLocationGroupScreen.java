package com.fbMatcher.screens;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class KickaroundLocationGroupScreen extends KickaroundBaseScreen{
	
	//constructor from superclass
	public KickaroundLocationGroupScreen(AppiumDriver<MobileElement> driver) {
		super(driver);
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	}
	
	//locating the web elements on Kickaround Location and Group Screen
	@FindBy(xpath="//div[@class='page-title']")
	public WebElement titleGame;
	
	@FindBy(xpath="//div[@class='page-subtitle next-game-text']/span[1]")
	public WebElement subTitleGameFrequency;
	
	@FindBy(xpath="//div[@class='page-subtitle next-game-text']/span[2]")
	public WebElement subTitleGameStatus;
	
	@FindBy(xpath="//a[@class='link-with-no-background'][text()=' See Location & Group Info ']")
	public WebElement linkLatestGame;
	
	@FindBy(xpath="//mat-card-content[@class='mat-card-content']/fm-kickaround-Summary/div/div/div[1]/div/div/div[1]")
	public WebElement labelGameFrequency;
	
	@FindBy(xpath="//mat-card-content[@class='mat-card-content']/fm-kickaround-Summary/div/div/div[1]/div[2]/div/div")
	public WebElement labelCost;
	
	@FindBy(xpath="//mat-card-content[@class='mat-card-content']/fm-kickaround-Summary/div/div/div[2]/div[1]/div/div")
	public WebElement labelLevelGender;
	
	@FindBy(xpath="//mat-card-content[@class='mat-card-content']/fm-kickaround-Summary/div/div/div[2]/div[2]/div/div")
	public WebElement labelPitchSize;
	
	@FindBy(xpath="//mat-card-content[@class='mat-card-content']/fm-kickaround-Summary/div/div[2]/div/div")
	public WebElement labelLocation;
	
	@FindBy(xpath="//mat-card-content/div[2]/div[2]/fm-group-summary/div[1]/h5")
	public WebElement countPlayersInGroup;
	
	@FindBy(xpath="//mat-card-content/div[2]/div[1]/fm-kickaround-share-join-link/div/h5")
	public WebElement headerInvite;
	
	@FindBy(xpath="//mat-card-content/div[2]/div[1]/fm-kickaround-share-join-link/div/div[1]/a")
	public WebElement linkJoinGame;
	
	//defining functionalities of the chosen Web elements
	public boolean isGameTitle() {
		return titleGame.isDisplayed();		
	}
	
	public boolean isGameFrequencyTitle() {
		return subTitleGameFrequency.isDisplayed();
	}
	
	public void clickLatestGame() {
		linkLatestGame.click();
	}
	
	public boolean isGameFrequencyLabel() {
		return labelGameFrequency.isDisplayed();
	}
	
	public boolean isCostLabel() {
		return labelCost.isDisplayed();
	}
	
	public boolean isLevelGenderLabel() {
		return labelLevelGender.isDisplayed();
	}
	
	public boolean isPitchSizeLabel( ) {
		return labelPitchSize.isDisplayed();
	}
	
	public boolean isLocationLabel() {
		return labelLocation.isDisplayed();
	}
	
	public boolean isPlayerCount() {
		return countPlayersInGroup.isDisplayed();
	}
	
	public boolean isInviteHeader() {
		return headerInvite.isDisplayed();
	}
	
	public void joinGame() {
		linkJoinGame.click();
	}
}
