package com.fbMatcher.screens;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class ProfileScreen extends KickaroundBaseScreen {

	// constructor from superclass
	public ProfileScreen(AppiumDriver<MobileElement> driver) {
		super(driver);
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	}

	// locating the web elements on Profile Screen
	@FindBy(xpath = "//fm-profile-image/div/div/form/div/div/div[2]/div")
	public WebElement imageProfile;

	@FindBy(xpath = "/html/body/div[2]/div[2]/div/div/div/button[1]")
	public WebElement updateProfilePic;

	@FindBy(xpath = "/html/body/div[2]/div[2]/div/div/div/button[2]")
	public WebElement removeProfilePic;
	
	@FindBy(xpath="//fm-profile/mat-card/div/div[1]/div")
	public WebElement linkCloseAccount;

	@FindBy(xpath = "//h5[text()='Account Details']")
	public WebElement headerAccountDetails;

	@FindBy(xpath = "//fm-update-user-details/form/div[1]/div[1]/div/div/div[1]/mat-form-field/div/div[1]/div/input")
	public WebElement textFirstName;
	
	@FindBy(xpath = "//fm-update-user-details/form/div[1]/div[1]/div/div/div[2]/mat-form-field/div/div[1]/div/input")
	public WebElement textLastName;

	@FindBy(xpath = "//fm-update-user-details/form/div[1]/div[1]/div/mat-form-field[1]/div/div[1]/div/input")
	public WebElement textEmailAddress;
	
	@FindBy(xpath = "//fm-update-user-details/form/div[1]/div[1]/div/mat-form-field[2]/div/div[1]/div/input")
	public WebElement textMobilePhone;
	
	@FindBy(xpath = "//fm-update-user-details/form/div[1]/div[2]/mat-slide-toggle/label/div/div/div[1]")
	public WebElement sliderTrackTrace;

	@FindBy(xpath = "//fm-update-user-details/form/div[2]/input[2][@value='Edit Details']")
	public WebElement buttonEditAccount;
	
	@FindBy(xpath = "//fm-update-user-details/form/div[2]/input[2][@value='Save Details']")
	public WebElement buttonSaveAccount;
	
	@FindBy(xpath = "//fm-update-user-details/form/div[2]/input[1]")
	public WebElement buttonChangePassword;

	@FindBy(xpath = "//mat-dialog-container/fm-change-password/h1")
	public WebElement headerChangePassword;

	@FindBy(xpath = "//mat-dialog-container/fm-change-password/div/form/div[1]/div[1]/mat-form-field/div/div[1]/div/input")
	public WebElement textExistingPassword;

	@FindBy(xpath = "//mat-dialog-container/fm-change-password/div/form/div[1]/div[2]/mat-form-field/div/div[1]/div/input")
	public WebElement textChangePassword;

	@FindBy(xpath = "//mat-dialog-container/fm-change-password/div/form/div[1]/div[3]/mat-form-field/div/div[1]/div/input")
	public WebElement textConfirmPassword;

	@FindBy(xpath = "//mat-dialog-container/fm-change-password/div/form/div[2]/input[1]")
	public WebElement buttonCancelPassword;
	
	@FindBy(xpath = "//mat-dialog-container/fm-change-password/div/form/div[2]/input[2]")
	public WebElement buttonUpdatePassword;
	
	
	// defining functionalities of the chosen Web elements
	public boolean isImageProfile() {
		return imageProfile.isDisplayed();
	}

	public void clickProfilePic() {
		imageProfile.click();
	}

	public void clickProfilePicUpdate() {
		updateProfilePic.click();
	}

	public void clickProfileRemove() {
		removeProfilePic.click();
	}

	public void clickCloseAccount() {
		linkCloseAccount.click();
	}

	public boolean isHeaderUpdateDetails() {
		return headerAccountDetails.isDisplayed();
	}

	public void setFirstName(String firstName) {
		textFirstName.sendKeys(firstName);
	}
	
	public void setLastName(String lastName) {
		textLastName.sendKeys(lastName);
	}

	public void setEmailAddress(String email) {
		textEmailAddress.sendKeys(email);
	}
	
	public void setMobleNo(String mobileNo) {
		textMobilePhone.sendKeys(mobileNo);
	}
	
	public void checkTrackTrace(String state) {
		if (state == "No") {
			sliderTrackTrace.click();
		}
	}

	public void clickEditAccount() {
		buttonEditAccount.click();
	}
	
	public void clickSaveAccount() {
		buttonSaveAccount.click();
	}
	
	public void clickChangePassword() {
		buttonChangePassword.click();
	}

	public boolean isHeaderChangePsw() {
		return headerChangePassword.isDisplayed();
	}

	public void setExistingPsw(String existPassword) {
		textExistingPassword.sendKeys(existPassword);
	}

	public void setChangePsw(String newPassword) {
		textChangePassword.sendKeys(newPassword);
	}

	public void setConfirmPsw(String confirmPassword) {
		textConfirmPassword.sendKeys(confirmPassword);
	}

	public void clickCancelPassword() {
		buttonCancelPassword.click();
	}
	
	public void clickUpdatePasswordCancel() {
		buttonUpdatePassword.click();
	}
	
}
