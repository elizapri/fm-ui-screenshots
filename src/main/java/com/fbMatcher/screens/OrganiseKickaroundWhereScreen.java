package com.fbMatcher.screens;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.fbMatcher.base.ScreenBase;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class OrganiseKickaroundWhereScreen extends ScreenBase {

	// constructor from superclass
	public OrganiseKickaroundWhereScreen(AppiumDriver<MobileElement> driver) {
		super(driver);
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	}

	// locating the web elements on Organise Kickaround - Where Screen
	@FindBy(xpath = "//div[@class='mat-step-label mat-step-label-active mat-step-label-selected']/div[contains(text(),'Where')]")
	public WebElement titleWhere;

	@FindBy(xpath = "(//button[@type='button'][text()='Back'])[2]")
	public WebElement buttonGoBack;

	@FindBy(xpath = "(//button[@type='submit'][text()='Next '])[3]")
	public WebElement buttonNext;

	@FindBy(xpath = "//input[@formcontrolname='searchOpenDataLocation'][@role='combobox']")
	public WebElement textLocation;

	@FindBy(xpath = "//div[@role='listbox']")
	public WebElement listLocations;

	// defining functionalities of the chosen Web elements
	public boolean isTitleWhen() {
		return titleWhere.isDisplayed();
	}

	public void clickGoBack() {
		buttonGoBack.click();
	}

	public void clickNext() {
		buttonNext.click();
	}

	public void setLocation(String gameLocation) {
		textLocation.clear();
		textLocation.sendKeys(gameLocation);
	}

	public void selectLocation(String gameLocation) {
		List<WebElement> optionsToSelect = listLocations.findElements(By.tagName("span"));
		for (WebElement option : optionsToSelect) {
			if (option.getText().equals(gameLocation)) {
				option.click();
				break;
			}
		}
	}
}
