package com.fbMatcher.screens;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.fbMatcher.base.ScreenBase;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class OrganiseKickaroundWhenScreen extends ScreenBase {

	// constructor from superclass
	public OrganiseKickaroundWhenScreen(AppiumDriver<MobileElement> driver) {
		super(driver);
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	}

	// locating the web elements on Organise Kickaround - When Screen
	@FindBy(xpath="//div[@class='mat-step-label mat-step-label-active mat-step-label-selected']/div[contains(text(),'When')]")
	public WebElement titleWhen;
	
	@FindBy(xpath="(//button[@type='button'][text()='Back'])[1]")
	public WebElement buttonGoBack;
	
	@FindBy(xpath="(//button[@type='submit'][text()='Next '])[2]")
	public WebElement buttonNext;
	
	@FindBy(xpath="//mat-select[@formcontrolname='frequency'][@placeholder='How often do you play?']")
	public WebElement drpFrequency;
	
	@FindBy(xpath="//input[@formcontrolname='duration'][@data-placeholder='Usual Duration (mins)']")
	public WebElement textDuration;
	
	@FindBy(xpath= "//mat-checkbox/label/div/input[@type='checkbox'][@aria-checked='false']")
	public WebElement checkboxCreateGame;
	
	@FindBy(xpath="//input[@formcontrolname='startDate'][@data-placeholder='When is your first/next game?']")
	public WebElement dateNextGame;
	
	@FindBy(xpath="//button[@type='button'][@aria-label='Open calendar']")
	public WebElement buttonCalendar;
	
	@FindBy(xpath="//button[@type='button'][@aria-label='Choose month and year']")
	public WebElement buttonMonthYear;
		
	@FindBy(xpath="//input[@type='text'][@aria-label='Hours']")
	public WebElement textHours;
	
	@FindBy(xpath="//input[@type='text'][@aria-label='Minutes']")
	public WebElement textMinutes;
	
	@FindBy(xpath="//button[@type='button'][@class='btn btn-outline-primary']")
	public WebElement buttonAMPM;
	
	@FindBy(xpath = "//div[@role='listbox']")
	public WebElement overlayOptions;
	
	//defining functionalities of the chosen Web elements
	public boolean isTitleWhen() {
		return titleWhen.isDisplayed();
	}
	
	public void clickGoBack() {
		buttonGoBack.click();
	}
	
	public void clickNext() {
		buttonNext.click();
	}
	
	public void clickGameFrequency() {
		drpFrequency.click();
	}
	
	public void setGameDuration(String gameDuration) {
		textDuration.clear();
		textDuration.sendKeys(gameDuration);
	}
	
	public void uncheckCreateGame() {
		if (checkboxCreateGame.isSelected() == true) {
			checkboxCreateGame.click();	
		}
	}
	
	public void checkCreateGame() {
		if (checkboxCreateGame.isSelected() == false) {
			checkboxCreateGame.click();	
		}
	}
	
	public void clickCalendar() {
		buttonCalendar.click();
	}
	
	public void selectDate(String gameYear, String gameMonth, String gameDay) {
		buttonMonthYear.click();
		driver.findElement(By.xpath("//mat-multi-year-view/table/tbody/tr/td/div[contains(text(),"+gameYear+")]")).click();
		driver.findElement(By.xpath("//mat-year-view/table/tbody/tr/td/div[text()[contains(.,'"+gameMonth+"')]]")).click();
		driver.findElement(By.xpath("//mat-month-view/table/tbody/tr/td/div[contains(text(),"+gameDay+")]")).click();
	}
	
	public void setGameHours(String gameHours) {
		textHours.clear();
		textHours.sendKeys(gameHours);
	}
	
	public void setGameMinutes(String gameMinutes) {
		textMinutes.clear();
		textMinutes.sendKeys(gameMinutes);
	}
	
	public void clickAMPM(String ampm) {
		if(buttonAMPM.getText() != ampm) {
			buttonAMPM.click();
		}		
	}
	
	public void selectOption(String valueOption) {
		List<WebElement> optionsToSelect = overlayOptions.findElements(By.tagName("span"));
		for (WebElement option : optionsToSelect) {
			if (option.getText().equals(valueOption)) {
				option.click();
				break;
			}
		}
	}
}
