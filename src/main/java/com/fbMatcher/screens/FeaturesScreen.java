package com.fbMatcher.screens;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.fbMatcher.base.ScreenBase;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;


public class FeaturesScreen extends ScreenBase{
	
	//constructor from superclass
	public FeaturesScreen(AppiumDriver<MobileElement> driver) {
		super(driver);
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	}
	
	//locating the web elements on Features Screen
	@FindBy(xpath="//*[@id='features']/div/div[1]/div/div/div[1]")
	public WebElement titleFeatures;
	
	@FindBy(xpath="//*[@id=\"features\"]/div/div[2]/div[2]/div[3]/div[2]")
	public WebElement checkpointFeature;
	
	@FindBy(xpath="//fm-features/section/div/div[3]/div/picture/img")
	public By imgMobile;
	
	//defining functionalities of the chosen Web elements	
	public boolean isTitleFeatures() {
		return titleFeatures.isDisplayed();
	}		
}