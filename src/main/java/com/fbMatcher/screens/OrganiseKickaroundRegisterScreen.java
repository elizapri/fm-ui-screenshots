package com.fbMatcher.screens;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.fbMatcher.base.ScreenBase;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class OrganiseKickaroundRegisterScreen extends ScreenBase {

	// constructor from superclass
	public OrganiseKickaroundRegisterScreen(AppiumDriver<MobileElement> driver) {
		super(driver);
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	}

	// locating the web elements on Organise Kickaround - Register Screen
	@FindBy(xpath = "//div[@class='mat-step-label mat-step-label-active mat-step-label-selected'/div[contains(text(),'Register')]")
	public WebElement titleRegister;

	@FindBy(xpath = "(//button[@type='button'][text()='Back'])[4]")
	public WebElement buttonGoBack;

	@FindBy(xpath = "//button[text()='Create ']")
	public WebElement buttonCreate;

	@FindBy(xpath = "//button[@type='button'][@class='socialloginBtn socialloginBtn--facebook']")
	public WebElement buttonFacebook;

	@FindBy(xpath = "//button[@type='button'][@class='socialloginBtn socialloginBtn--google']")
	public WebElement buttonGoogle;

	@FindBy(xpath = "//input[@type='text][@formcontrolname='firstName']")
	public WebElement textFirstName;

	@FindBy(xpath = "//input[@type='text][@formcontrolname='lastName']")
	public WebElement textLastName;

	@FindBy(xpath = "//input[@type='email'][@formcontrolname='email']")
	public WebElement textEmail;

	@FindBy(xpath = "//input[@type='password'][@formcontrolname='password']")
	public WebElement textPassword;

	@FindBy(xpath = "//input[@type='password'][@formcontrolname='confirmPassword']")
	public WebElement textConfirmPassword;

	// defining functionalities of the chosen Web elements
	public boolean isTitleRegister() {
		return titleRegister.isDisplayed();
	}

	public void clickGoBack() {
		buttonGoBack.click();
	}

	public void clickCreate() {
		buttonCreate.click();
	}

	public void clickFaceBook() {
		buttonFacebook.click();
	}

	public void clickGoogle() {
		buttonGoogle.click();
	}

	public void setFirstName(String firstName) {
		textFirstName.sendKeys(firstName);
	}

	public void setLastName(String lastName) {
		textLastName.sendKeys(lastName);
	}

	public void setEmail(String email) {
		textEmail.sendKeys(email);
	}

	public void setPassword(String password) {
		textPassword.sendKeys(password);
	}

	public void setConfirmPassword(String confirmPassword) {
		textConfirmPassword.sendKeys(confirmPassword);
	}

}
