package com.fbMatcher.screens;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.fbMatcher.base.ScreenBase;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class FindLocalFootBallGetNotifiedScreen extends ScreenBase {

	// constructor from superclass
	public FindLocalFootBallGetNotifiedScreen(AppiumDriver<MobileElement> driver) {
		super(driver);
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	}

	// locating the web elements on Find Game Notification Screen
	@FindBy(xpath = "//div[3]/div[contains(text(),'Get Notified')]")
	public WebElement titleNotify;

	@FindBy(xpath = "//form/div/div[1]/h3[contains(text(),'Sign up')]")
	public WebElement titleSignUp;

	@FindBy(xpath = "//form/div/div[1]/div[1]/div[1]/mat-form-field/div/div[1]/div/input[@data-placeholder='First Name']")
	public WebElement textFirstName;

	@FindBy(xpath = "//form/div/div[1]/div[1]/div[2]/mat-form-field/div/div[1]/div/input[@data-placeholder='Last Name']")
	public WebElement textLastName;

	@FindBy(xpath = "//form/div/div[1]/div[2]/div/mat-form-field/div/div[1]/div/input[@data-placeholder='Email address']")
	public WebElement textEmail;

	@FindBy(xpath = "//form/div/div[2]/h3[contains(text(),'Notify me for')]")
	public WebElement titleNotifyFor;

	@FindBy(xpath = "//form/div/div[2]/div/div[1]/mat-checkbox/label/div/input")
	public WebElement checkboxCasualGames;

	@FindBy(xpath = "//form/div/div[2]/div/div[2]/mat-checkbox/label/div/input")
	public WebElement checkboxCompetitions;

	@FindBy(xpath = "//form/div/div[2]/div/div[3]/mat-checkbox/label/div/input")
	public WebElement checkboxLeaguesTournaments;

	@FindBy(xpath = "(//button[@type='button'][contains(text(),'Back')])[2]")
	public WebElement buttonBack;

	@FindBy(xpath = "//button[@type='submit'][contains(text(),'Sign up')]")
	public WebElement buttonSignUp;

	// defining functionalities of the chosen Web elements
	public boolean isTitleNotify() {
		return titleNotify.isDisplayed();
	}

	public void clickTitleNotify() {
		titleNotify.click();
	}

	public boolean isTitleSignUp() {
		return titleSignUp.isDisplayed();
	}

	public void setFirstName(String firstName) {
		textFirstName.sendKeys(firstName);
	}

	public void setLastName(String lastName) {
		textLastName.sendKeys(lastName);
	}

	public void setEmail(String email) {
		textEmail.sendKeys(email);
	}

	public boolean isTitleNotifyFor() {
		return titleNotifyFor.isDisplayed();
	}

	public void uncheckCasualGames() {
		if (checkboxCasualGames.isSelected() == true) {
			checkboxCasualGames.click();
		}
	}

	public void checkCasualGames() {
		if (checkboxCasualGames.isSelected() == false) {
			checkboxCasualGames.click();
		}
	}

	public void uncheckCompetitions() {
		if (checkboxCompetitions.isSelected() == true) {
			checkboxCompetitions.click();
		}
	}

	public void checkCompetitions() {
		if (checkboxCompetitions.isSelected() == false) {
			checkboxCompetitions.click();
		}
	}

	public void uncheckLeaguesTournaments() {
		if (checkboxLeaguesTournaments.isSelected() == true) {
			checkboxLeaguesTournaments.click();
		}
	}

	public void checkLeaguesTournaments() {
		if (checkboxLeaguesTournaments.isSelected() == false) {
			checkboxLeaguesTournaments.click();
		}
	}

	public void clickBack() {
		buttonBack.click();
	}

	public void clickSignUp() {
		buttonSignUp.click();
	}
}
