package com.fbMatcher.screens;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.fbMatcher.base.ScreenBase;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class LandingHomeScreen extends ScreenBase{
	
	//constructor from superclass
	public LandingHomeScreen(AppiumDriver<MobileElement> driver) {
		super(driver);
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
		//logic for waiting
	}
	
	//locating the web elements on Landing screen
	@FindBy(xpath= "//*[@id='splash']/div/div/div[1]")
	public WebElement headerMain;
		
	@FindBy(xpath="//*[@id='navbarSupportedContent']/ul/li[1]/a")
	public WebElement linkHome;
	
	@FindBy(xpath="//*[@id='navbarSupportedContent']/ul/li[2]/a")
	public WebElement linkFeature;
	
	@FindBy(xpath="//*[@id='navbarSupportedContent']/ul/li[3]/a")
	public WebElement linkHowItWorks;
	
	@FindBy(xpath="//*[@id='navbarSupportedContent']/ul/li[4]/a")
	public WebElement linkBlogs;
	
	@FindBy(xpath="//*[@id='navbarSupportedContent']/ul/li[5]/a")
	public WebElement linkAboutUs;
	
	@FindBy(xpath="//*[@id='navbarSupportedContent']/ul/li[6]/a")
	public WebElement linkLogin;
	
	@FindBy(xpath="//*[@id='splash']/div/div/div[3]/button[1]")
	public WebElement buttonOrganise;
	
	@FindBy(xpath="//*[@id='splash']/div/div/div[3]/button[2]")
	public WebElement buttonFind;
	
	//defining functionalities of the chosen Web elements
	public boolean isHeaderMain() {
		return headerMain.isDisplayed();
	} 
		
	public void clickHome() {
		linkHome.click();
	}
	
	public void clickFeature() {
		linkFeature.click();
	}
	
	public void clickHowItWorks() {
		linkHowItWorks.click();
	}
	
	public void clickBlogs() {
		linkBlogs.click();
	}
	
	public void clickAboutUs() {
		linkAboutUs.click();		
	}
	
	public void clickLogin() {
		linkLogin.click();
	}	
	
	public void clickOrganiseGame() {
		buttonOrganise.click();
	}
	
	public void clickFindGame() {
		buttonFind.click();
	}

}

