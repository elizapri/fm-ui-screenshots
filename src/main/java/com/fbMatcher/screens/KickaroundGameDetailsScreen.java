package com.fbMatcher.screens;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class KickaroundGameDetailsScreen extends KickaroundBaseScreen{
	
	//constructor from superclass
		public KickaroundGameDetailsScreen(AppiumDriver<MobileElement> driver) {
			super(driver);
			PageFactory.initElements(new AppiumFieldDecorator(driver), this);
		}

		//locating the web elements on Game Details Screen
		@FindBy(xpath="//div[@class='top-banner-subtitle-above ng-star-inserted']")
		public WebElement titleCongratulate;
		
		@FindBy(xpath="//div[@class='top-banner-title']")
		public WebElement titleKickaroundName;
		
		@FindBy(xpath="//div[@class='top-banner-subtitle-below ng-star-inserted']")
		public WebElement dateNextGame;
		
		@FindBy(xpath="//mat-card-content/fm-game-top-banner/div[2]/div/div/div[3]/span")
		public WebElement dateToInvite;
		
		@FindBy(xpath="//a[@class='link-with-no-background'][text()=' See Latest Game ']")
		public WebElement linkLocationGroup;
		
		@FindBy(xpath="//mat-card-content/div[2]/div[2]/fm-kickaround-whatsapp-group/div/a")
		public WebElement linkJoinWhatsAppGroup;
		
		//defining functionalities of the chosen Web elements
		public boolean isKickroundNameTitle() {
			return titleKickaroundName.isDisplayed();
		}
		
		public boolean isNextGameDate() {
			return dateNextGame.isDisplayed();
		}
		
		public boolean isInviteDate() {
			return dateToInvite.isDisplayed();
		}
		
		public void clickLocationGroup() {
			linkLocationGroup.click();
		}
		
		public void clickJoinWhatsAppGroup() {
			linkJoinWhatsAppGroup.click();
		}	
}

