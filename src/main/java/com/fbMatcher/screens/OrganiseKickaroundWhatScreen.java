package com.fbMatcher.screens;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.fbMatcher.base.ScreenBase;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class OrganiseKickaroundWhatScreen extends ScreenBase {

	// constructor from superclass
	public OrganiseKickaroundWhatScreen(AppiumDriver<MobileElement> driver) {
		super(driver);
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	}

	// locating the web elements on Organise Kickaround - What Screen
	@FindBy(xpath = "//div[@class='mat-step-label mat-step-label-active mat-step-label-selected']/div[contains(text(),'What')]")
	public WebElement titleWhat;

	@FindBy(xpath = "//fm-back-button/div/button")
	public WebElement buttonGoBack;

	@FindBy(xpath = "//div[2]/div/button[@type='submit']")
	public WebElement buttonNext;

	@FindBy(xpath = "//input[@formcontrolname='name'][@data-placeholder='Name of Kickaround* (max 25)']")
	public WebElement textKickaroundName;

	@FindBy(xpath = "//mat-select[@formcontrolname='whatsAppChoice'][@placeholder='WhatsApp Group Integration']")
	public WebElement drpWhatsAppGroup;

	@FindBy(xpath = "//input[@formcontrolname='existingWhatsAppUrl'][@data-placeholder='WhatsApp Group Join URL']")
	public WebElement textWhatsAppURL;

	@FindBy(xpath = "//mat-select[@formcontrolname='pitchPlayersPerSide'][@placeholder='Pitch Size*']")
	public WebElement drpPitchSize;

	@FindBy(xpath = "//mat-select[@formcontrolname='pitchType'][@placeholder='Pitch Type*']")
	public WebElement drpPitchType;

	@FindBy(xpath = "//input[@formcontrolname='minRequiredPlayers'][@data-placeholder='Min Required players']")
	public WebElement textMinPlayers;

	@FindBy(xpath = "//input[@formcontrolname='maxRequiredPlayers'][@data-placeholder='Max Required players']")
	public WebElement textMaxPlayers;

	@FindBy(xpath = "//mat-select[@formcontrolname='gender'][@placeholder='Gender']")
	public WebElement drpGender;

	@FindBy(xpath = "//mat-select[@formcontrolname='skillLevel'][@placeholder='Skill Level']")
	public WebElement drpSkillLevel;
	
	@FindBy(xpath = "//div[3]/mat-checkbox/label/div/input[@type='checkbox']")
	public WebElement checkboxNameMobile;
	
	@FindBy(xpath = "//div[@role='listbox']")
	public WebElement overlayOptions;

	// defining functionalities of the chosen Web elements
	public boolean isTitleWhat() {
		return titleWhat.isDisplayed();
	}

	public void clickGoBack() {
		buttonGoBack.click();
	}

	public void clickNext() {
		buttonNext.click();
	}

	public void setKickaroundName(String kickaroundName) {
		textKickaroundName.clear();
		textKickaroundName.sendKeys(kickaroundName);
	}

	public void clickWhatsAppIntegration() {
		drpWhatsAppGroup.click();
	}	

	public void setWhatsAppURL(String urlWhatsApp) {
		textWhatsAppURL.clear();
		textWhatsAppURL.sendKeys(urlWhatsApp);
	}

	public void clickPitchSize() {
		drpPitchSize.click();
	}

	public void clickPitchType() {
		drpPitchType.click();
	}

	public void setMinPlayers(String minPlayers) {
		textMinPlayers.clear();
		textMinPlayers.sendKeys(minPlayers);
	}

	public void setMaxPlayers(String maxPlayers) {
		textMaxPlayers.clear();
		textMaxPlayers.sendKeys(maxPlayers);
	}

	public void clickGender() {
		drpGender.click();
	}

	public void clickSkillLevel() {
		drpSkillLevel.click();
	}
	
	public void uncheckNameMobile() {
		if (checkboxNameMobile.isSelected() == true) {
			checkboxNameMobile.click();	
		}
	}
	
	public void checkNameMobile() {
		if (checkboxNameMobile.isSelected() == false) {
			checkboxNameMobile.click();	
		}
	}
	
	public void selectOption(String valueOption) {
		List<WebElement> optionsToSelect = overlayOptions.findElements(By.tagName("span"));
		for (WebElement option : optionsToSelect) {
			if (option.getText().equals(valueOption)) {
				option.click();
				break;
			}
		}
	}
}
