package com.fbMatcher.screens;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.fbMatcher.base.ScreenBase;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;


public class BlogsScreen extends ScreenBase {
	
//constructor from superclass
	public BlogsScreen(AppiumDriver<MobileElement> driver) {
		super(driver);
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	}
	
	//locating the web elements on About Us Screen
	@FindBy(xpath="//div[@class='blog-title']")
	public WebElement titleBlog;
		
	@FindBy(xpath="//span[@class='ng-star-inserted'][1]/a")
	public WebElement breadcrumbBlogHome;
		
	@FindBy(xpath="//span[@class='ng-star-inserted'][2]/a")
	public WebElement breadcrumbBlogs;
	
	//defining functionalities of the chosen Web elements		
	public boolean isTitleBlog() {
		return titleBlog.isDisplayed();
	}
		
	public void clickBreadcrumbBlogHome() {
		breadcrumbBlogHome.click();
	}
		
	public void clickBreadcrumbBlogs() {
		breadcrumbBlogs.click();
	}
}