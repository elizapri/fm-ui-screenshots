package com.fbMatcher.screens;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.fbMatcher.base.ScreenBase;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class LoginScreen extends ScreenBase {
		
	//constructor from superclass
	public LoginScreen(AppiumDriver<MobileElement> driver) {
		super(driver);
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	}
		
	//locating the web elements on Login Screen
	@FindBy(xpath="//div[@class='panel']/h3")
	public WebElement titleWelcome;
	
	@FindBy(xpath="//div[@class='auth-floating-panel']/login-form/form/fm-facebook-submit-button/div/button")
	public WebElement buttonFaceBookLogin;
	
	@FindBy(xpath="//div[@class='auth-floating-panel']/login-form/form/fm-google-submit-button/div/button")
	public WebElement buttonGoogleLogin;
	
	@FindBy(xpath="//input[@id='username']")
	public WebElement textEmail;
	
	@FindBy(xpath="//input[@id='password']")
	public WebElement textPassword;
	
	@FindBy(xpath="//input[@type='checkbox']")
	public WebElement checkboxRemember;
	
	@FindBy(xpath="//div[@class='auth-floating-panel']/login-form/form/div[2]/div[2]/a")
	public WebElement linkForgottenPsw;
	
	@FindBy(xpath="//fm-email-submit-button/div/button")
	public WebElement buttonLogin;
	
	
	@FindBy(xpath="//div[@class='auth-floating-panel']/login-form/div/p[1]/a")
	public WebElement linkSignUp;
	
	//defining functionalities of the chosen Web elements	
	public boolean isTitleWelcome() {
		return titleWelcome.isDisplayed();
	}
	
	public void clickFaceBookLogin() {
		buttonFaceBookLogin.click();
	}
	
	public void clickGoogleLogin() {
		buttonGoogleLogin.click();
	}
	
	public void setEmail(String email) {
		textEmail.sendKeys(email);
	}
	
	public void setPassword(String password) {
		textPassword.sendKeys(password);
	}	
	
	public void checkRemember(String state) {
		if (state == "Yes") {
		checkboxRemember.click();	
		}
	}
	
	public void clickForgottenPsw() {
		linkForgottenPsw.click();
	}
	
	public void clickLogin() {
		buttonLogin.click();
	}
	
	public void clickSignUp() {
		linkSignUp.click();
	}
	
	
}
