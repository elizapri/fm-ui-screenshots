package com.fbMatcher.screens;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.fbMatcher.base.ScreenBase;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class HowItWorksScreen extends ScreenBase{
	
	//constructor from superclass
	public HowItWorksScreen(AppiumDriver<MobileElement> driver) {
		super(driver);
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
}
	//locating the web elements on Features Screen
	@FindBy(xpath="//*[@id='how-it-works']/div/div[1]/div/div/div")
	public WebElement titleHowItWorks;
	
	@FindBy(xpath="//*[@id='how-it-works']/div/div[2]/div[1]/div/div/div[1]")
	public WebElement linkCreateGroup;
	
	@FindBy(xpath="//*[@id='how-it-works']/div/div[2]/div[1]/div/div/div[2]")
	public WebElement linkShareGroup ;
	
	@FindBy(xpath="//*[@id='how-it-works']/div/div[2]/div[1]/div/div/div[3]")
	public WebElement linkSendInvites;
	
	@FindBy(xpath="//*[@id='how-it-works']/div/div[2]/div[1]/div/div/div[4]")
	public WebElement linkConfirmGame ;
	
	@FindBy(xpath="//*[@id='how-it-works']/div/div[2]/div[1]/div/div/div[5]")
	public WebElement linkPlayGetPaid;
	
	@FindBy(xpath="//*[@id='slots-into-anywhere']/div/div[1]/div/div/div")
	public WebElement titleSlotOrganiseNow;
	
	@FindBy(xpath="//*[@id='call-to-action']/div/div[1]/div/div/div[1]")
	public WebElement titleNeedFB;
	
	@FindBy(xpath="//*[@id='call-to-action']/div/div[2]/div/button")
	public WebElement buttonMatchMe;
	
	@FindBy(xpath="//*[@id='blog-section']/div/div[1]/div/div/div")
	public WebElement titleLatestArticles;
	
	//defining functionalities of the chosen Web elements	
	public boolean isTitleHowItWorks() {
		return titleHowItWorks.isDisplayed();
	}
	
	public void clickCreateGroup() {
		linkCreateGroup.click();
	}
	
	public void clickShareGroup() {
		linkShareGroup.click();
	}	
	
	public void clickSendInvites() {
		linkSendInvites.click();
	}	
	
	public void clickConfirmGame() {
		linkConfirmGame.click();
	}	
	
	public void clickPlayGetPaid() {
		linkPlayGetPaid.click();
	}
	
	public boolean isTitleSlotOrganiseNow() {
		return titleSlotOrganiseNow.isDisplayed();
	}
	
	public boolean isTitleNeedFB() {
		return titleNeedFB.isDisplayed();
	}
	
	public void clickMatchMe() {
		buttonMatchMe.click();
	}
}
