package com.fbMatcher.screens;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class KickaroundScreen extends KickaroundBaseScreen {
	
	//constructor from superclass
	public KickaroundScreen(AppiumDriver<MobileElement> driver) {
		super(driver);
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	}
	
	//locating the web elements on Kickaround Screen
	@FindBy(xpath="//mat-card[@class='mat-card']")
	public WebElement cardKickarounds;
	
	@FindBy(xpath="//div[@class='mat-card-title-wrapper pending']/div/div[2]/div[@class='fm-card-title link-with-background']")
	public List<WebElement> listKickaroundTitles;
	
	@FindBy(xpath="//div[@class='mat-card-title-wrapper pending']/div/div[2]/div[@class='fm-card-subtitle link-with-background']")
	public List<WebElement> listGames;
	
	@FindBy(xpath="//div[@class='mat-card-content-wrapper']")
	public List<WebElement> listKickaroundContents;	
	
	//defining functionalities of the chosen Web elements
	public void clickKickaroundTitle(String nameKickaround) {
		for (WebElement linkKickaroundTitle : listKickaroundTitles) {
			System.out.println(linkKickaroundTitle.getText());
			if (linkKickaroundTitle.getText() == nameKickaround) {
				System.out.println("Matched");
				linkKickaroundTitle.click();				
			}
		}
	}
	
	public void clickKickaroundGame(String nameKickaround) {
		WebElement linkGame = driver.findElementByXPath("//div[contains(text(),"+"nameKickaround)]/parent::div//following-sibling::div[@class='fm-card-subtitle link-with-background']");
		linkGame.click();
	}		
}