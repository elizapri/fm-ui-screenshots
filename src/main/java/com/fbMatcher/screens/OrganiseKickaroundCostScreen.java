package com.fbMatcher.screens;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;


import com.fbMatcher.base.ScreenBase;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class OrganiseKickaroundCostScreen extends ScreenBase {

	// constructor from superclass
	public OrganiseKickaroundCostScreen(AppiumDriver<MobileElement> driver) {
		super(driver);
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	}

	// locating the web elements on Organise Kickaround - Cost Screen
	@FindBy(xpath = "//div[@class='mat-step-label mat-step-label-active mat-step-label-selected']")
	public WebElement titleCost;

	@FindBy(xpath = "(//button[@type='button'][text()='Back'])[3]")
	public WebElement buttonGoBack;

	@FindBy(xpath = "//button[text()='Create ']")
	public WebElement buttonCreate;
	
	@FindBy(xpath = "(//button[@type='submit'][text()='Next '])[4]")
	public WebElement buttonNext;

	@FindBy(xpath = "//mat-select[@formcontrolname='paymentMethod'][@placeholder='How do you want to collect player money?']")
	public WebElement drpHowCollect;

	@FindBy(xpath = "//mat-select[@formcontrolname='playerPricingMode'][@placeholder='Fixed or variable player price?']")
	public WebElement drpHowPriceType;

	@FindBy(xpath = "//input[@formcontrolname='fixedPlayerPrice'][@data-placeholder='Fixed player price']")
	public WebElement textFixedCost;

	@FindBy(xpath = "//input[@formcontrolname='price'][@data-placeholder='Pitch hire cost']")
	public WebElement textVariableCost;

	@FindBy(xpath = "//input[@formcontrolname='discountCode'][@data-placeholder='Enter coupon code']")
	public WebElement textDiscountCode;

	@FindBy(xpath = "//strong[text()='Price Breakdown']")
	public WebElement titlePriceBreakdown;

	@FindBy(xpath = "//fm-pricer-fixed/p[2]/strong[1]")
	public WebElement displayFixedMinPlayer;

	@FindBy(xpath = "//fm-pricer-fixed/p[2]/strong[2]")
	public WebElement displayFixedMinCost;

	@FindBy(xpath = "//fm-pricer-fixed/p[3]/strong[1]")
	public WebElement displayFixedMaxPlayer;

	@FindBy(xpath = "//fm-pricer-fixed/p[3]/strong[2]")
	public WebElement displayFixedMaxCost;

	@FindBy(xpath = "//fm-pricer-dynamic/p[2]/strong[1]")
	public WebElement displayVariableMinPlayer;

	@FindBy(xpath = "//fm-pricer-dynamic/p[2]/strong[2]")
	public WebElement displayVariableMinCost;

	@FindBy(xpath = "//fm-pricer-dynamic/p[3]/strong[1]")
	public WebElement displayVariableMaxPlayer;

	@FindBy(xpath = "//fm-pricer-dynamic/p[3]/strong[2]")
	public WebElement displayVariableMaxCost;
	
	@FindBy(xpath = "//div[@role='listbox']")
	public WebElement overlayOptions;

	// defining functionalities of the chosen Web elements
	public boolean isTitleCost() {
		return titleCost.isDisplayed();
	}

	public void clickGoBack() {
		buttonGoBack.click();
	}

	public void clickCreate() {
		buttonCreate.click();
	}
	
	public void clickNext() {
		buttonNext.click();
	}

	public void clickHowToCollect() {
		drpHowCollect.click();
	}

	public void clickPriceType() {
		drpHowPriceType.click();
	}

	public void setFixedCost(String fixedCost) {
		textFixedCost.clear();
		textFixedCost.sendKeys(fixedCost);
	}

	public void setVariableCost(String variableCost) {
		textVariableCost.clear();
		textVariableCost.sendKeys(variableCost);
	}

	public void setDiscountCode(String discountCode) {
		textDiscountCode.clear();
		textDiscountCode.sendKeys(discountCode);
	}

	public boolean isTitlePriceBreakdown() {
		return titlePriceBreakdown.isDisplayed();
	}

	public String getFixedMinPlayer() {
		return displayFixedMinPlayer.getText();
	}

	public String getFixedMaxPlayer() {
		return displayFixedMaxPlayer.getText();
	}

	public String getVariableMinPlayer() {
		return displayVariableMinPlayer.getText();
	}

	public String getVariableMaxPlayer() {
		return displayVariableMaxPlayer.getText();
	}

	public String getFixedMinCost() {
		return displayFixedMinCost.getText();
	}

	public String getFixedMaxCost() {
		return displayFixedMaxCost.getText();
	}

	public String getVariableMinCost() {
		return displayVariableMinCost.getText();
	}

	public String getVariableMaxCost() {
		return displayVariableMaxCost.getText();
	}
	
	public void selectOption(String valueOption) {
		List<WebElement> optionsToSelect = overlayOptions.findElements(By.tagName("span"));
		for (WebElement option : optionsToSelect) {
			if (option.getText().equals(valueOption)) {
				option.click();
				break;
			}
		}
	}
}
