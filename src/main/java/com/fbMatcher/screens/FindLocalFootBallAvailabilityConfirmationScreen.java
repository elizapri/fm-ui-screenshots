package com.fbMatcher.screens;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.fbMatcher.base.ScreenBase;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class FindLocalFootBallAvailabilityConfirmationScreen extends ScreenBase {

	// constructor from superclass
	public FindLocalFootBallAvailabilityConfirmationScreen(AppiumDriver<MobileElement> driver) {
		super(driver);
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	}

	// locating the web elements on Game Availability Confirmation Screen
	@FindBy(xpath = "//form/mat-card-content/div/fm-find-success/h3")
	public WebElement titleConfirmation;

	@FindBy(xpath = "//button[@type='submit'][contains(text(),'Close')]")
	public WebElement buttonClose;

	// defining functionalities of the chosen Web elements
	public boolean isTitleConfirmation() {
		return titleConfirmation.isDisplayed();
	}

	public void clickClose() {
		buttonClose.click();
	}
}
