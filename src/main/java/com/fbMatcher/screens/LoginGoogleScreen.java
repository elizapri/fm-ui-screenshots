package com.fbMatcher.screens;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.fbMatcher.base.ScreenBase;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class LoginGoogleScreen extends ScreenBase {

	// constructor from superclass
	public LoginGoogleScreen(AppiumDriver<MobileElement> driver) {
		super(driver);
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	}

	// locating the web elements on Google Login Screen
	@FindBy(xpath = "//div[text()='Sign in with Google']")
	public WebElement titleGoogle;

	@FindBy(xpath = "//button[@type='button'][text()='footballmatcher.io']")
	public WebElement linkFBMatcherSite;

	@FindBy(xpath = "//input[@type='email'][@aria-label='Email or phone']")
	public WebElement textEmail;

	@FindBy(xpath = "//button[@type='button']/span[text()='Next']")
	public WebElement buttonNextPsw;

	@FindBy(xpath = "//div[@id='profileIdentifier']")
	public WebElement titleProfileID;

	@FindBy(xpath = "//input[@type='password'][@name='password']")
	public WebElement textPassword;

	@FindBy(xpath = "//button[@type='button']/span[text()='Next']")
	public WebElement buttonNext;

	// defining functionalities of the chosen Web elements
	public boolean isTitleGoogle() {
		return titleGoogle.isDisplayed();
	}

	public void clickFBMatcherSite() {
		linkFBMatcherSite.click();
	}

	public void setEmail(String email) {
		textEmail.sendKeys(email);
	}

	public void clickNextPsw() {
		buttonNextPsw.click();
	}

	public boolean isTitleProfileID() {
		return titleProfileID.isDisplayed();
	}

	public void setPassword(String password) {
		textPassword.sendKeys(password);
	}

	public void clickNext() {
		buttonNext.click();
	}
}
