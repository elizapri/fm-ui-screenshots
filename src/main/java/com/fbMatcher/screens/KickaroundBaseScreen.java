package com.fbMatcher.screens;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.fbMatcher.base.ScreenBase;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class KickaroundBaseScreen extends ScreenBase {
		
	//constructor from superclass
	public KickaroundBaseScreen(AppiumDriver<MobileElement> driver) {
		super(driver);
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	}

	//locating the web elements on Kickaround Base Screen
	@FindBy(xpath="//a[@class='link-with-no-background']")
	public WebElement titleKickaround;
	
	@FindBy(xpath="//div[@class='wrapper']/div/h5")
	public WebElement msgKickaround;
	
	@FindBy(xpath="//p[@class='link-with-background']")
	public WebElement linkStartToOrganise;
	
	@FindBy(xpath="//img[@class='action-icon']")
	public WebElement imgOrganise;
	
	@FindBy(xpath="//div/fm-secure/fm-top-menu/nav/div/ul/li[1]/a")
	public WebElement linkKickaround;
	
	@FindBy(xpath="//div/fm-secure/fm-top-menu/nav/div/ul/li[2]/a")
	public WebElement linkUserName;
	
	@FindBy(xpath="//*[@id='navbarSupportedContent']/ul/li[1]/div/a[1]")
	public WebElement linkList;
	
	@FindBy(xpath="//*[@id='navbarSupportedContent']/ul/li[1]/div/a[2]")
	public WebElement linkCreate;
	
	@FindBy(xpath="//*[@id='navbarSupportedContent']/ul/li[2]/div/a[1]")
	public WebElement linkProfile;
	
	@FindBy(xpath="//*[@id='navbarSupportedContent']/ul/li[2]/div/a[2]")
	public WebElement linkPayments;
	
	@FindBy(xpath="//*[@id='navbarSupportedContent']/ul/li[2]/div/a[3]")
	public WebElement linkLogout;
	
	//Feedback available for Authenticated User on all screens
	@FindBy(xpath="//*[@id='slideout_btn']")
	public WebElement buttonFeedback;
	
	//defining functionalities of the chosen Web elements		
	public void clickTitleKickaround() {
		titleKickaround.click();
	}
	
	public boolean isKickaroundMsg() {
		return msgKickaround.isDisplayed();
	}
	
	public void startOrganise() {
		linkStartToOrganise.click();
	}
	
	public void startOrganiseAction() {
		imgOrganise.click();
	}
	
	public void clickMenuKickaround() {
		linkKickaround.click();
	}
	
	public void clickMenuList() {
		linkList.click();
	}
	
	public void createMenuKickaround() {
		linkCreate.click();
	}
	
	public void clickMenuUserName() {
		linkUserName.click();
	}
	
	public void clickMenuProfile() {
		linkProfile.click();
	}
	
	public void clickMenuPayments() {
		linkPayments.click();
	}
	
	public void clickLogout() {
		linkLogout.click();
	}
	
	public void clickFeedback() {
		buttonFeedback.click();
	}
}
