package com.fbMatcher.screens;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.fbMatcher.base.ScreenBase;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.touch.offset.PointOption;

public class FindLocalFootBallWhereScreen extends ScreenBase {

	// constructor from superclass
	public FindLocalFootBallWhereScreen(AppiumDriver<MobileElement> driver) {
		super(driver);
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	}

	// locating the web elements on Find Game Where Screen
	@FindBy(xpath = "//div[3]/div[contains(text(),'Where')]")
	public WebElement titleWhere;

	@FindBy(xpath = "//input[@placeholder='Enter a location']")
	public WebElement textLocation;

	@FindBy(xpath = "//mat-slider[@role='slider']")
	public WebElement sliderHowFar;

	@FindBy(xpath = "//form/div/div[2]/h3[contains(text(),'Your locations')]")
	public WebElement titleLocations;

	@FindBy(xpath = "//form/div/div[2]/fm-list-places")
	public WebElement tableLocations;

	@FindBy(xpath = "//fm-back-button/div/button[contains(text(), 'Go Back')]")
	public WebElement buttonGoBack;

	@FindBy(xpath = "(//button[contains(text(), 'Next')])[1]")
	public WebElement buttonNext;

	// defining functionalities of the chosen Web elements
	public boolean isTitleWhere() {
		return titleWhere.isDisplayed();
	}

	public void clickTitleWhere() {
		titleWhere.click();
	}

	public void setLocation(String location) {
		textLocation.sendKeys(location);
	}

	// need to review this login later
	public void slideMiles(String miles) {

		int startX = sliderHowFar.getLocation().getX();
		int yAxis = sliderHowFar.getLocation().getY();
		System.out.println("X is: " + startX + " Y is: " + yAxis);
		int moveToXDirectionAt = (int) Double.parseDouble(miles) + startX;
		int end = sliderHowFar.getSize().getWidth();
		System.out.println("End point is: " + end);
		System.out.println("Move to: " + moveToXDirectionAt);
		@SuppressWarnings("rawtypes")
		TouchAction action = new TouchAction(driver);
		action.longPress(PointOption.point(startX, yAxis)).moveTo(PointOption.point(moveToXDirectionAt, yAxis))
				.release().perform();
	}

	public boolean isTitleLocations() {
		return titleLocations.isDisplayed();
	}

	public boolean isTableLocations() {
		return tableLocations.isDisplayed();
	}

	public void clickGoBack() {
		buttonGoBack.click();
	}

	public void clickNext() {
		buttonNext.click();
	}
}
